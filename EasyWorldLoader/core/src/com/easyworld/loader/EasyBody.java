package com.easyworld.loader;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by rene__000 on 3/26/2015.
 */
public class EasyBody {
    public Vector2[] vertices;
    private TextureRegion textureRegion;
    private PolygonShape polygonShape;
    private PolygonRegion polygonRegion;

    public PolygonRegion GetPolygonRegion() {
        return polygonRegion;
    }

    public Body body;
    private String name;

    public String GetName() {
        return name;
    }

    ContactListener contactListener;

    public void setVertices(Vector2[] vertices) {
        this.vertices = vertices;
    }

    public Vector2[] getVertices() {
        return vertices;
    }

    public EasyBody(World world, BodyDef bodyDef, FixtureDef fixtureDef, String name) {
        body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        this.name = name;

    }

    public void SetTexture(String textureName, AssetManager assetManager, float cameraZoom) {
        polygonShape = (PolygonShape) body.getFixtureList().get(0).getShape();
        float[] polygonVertices = new float[vertices.length * 2];
        for (int i = 0, j = 0; i < polygonVertices.length; i++, j++) {
            polygonVertices[i] = vertices[j].x / cameraZoom;
            i++;
            polygonVertices[i] = vertices[j].y / cameraZoom;
        }
        short[] triangles = new EarClippingTriangulator()
                .computeTriangles(polygonVertices)
                .toArray();

        textureRegion = new TextureRegion(GetTextureAsset(textureName, assetManager));
        polygonRegion = new PolygonRegion(textureRegion, polygonVertices, triangles);

    }

    private Texture GetTextureAsset(String fileName, AssetManager assetManager) {
        return assetManager.get(fileName, Texture.class);
    }

    public Rectangle CalculateRoughBodyRectangle() {
        if (body.getFixtureList().get(0).getShape().getType() == Shape.Type.Polygon) {
            float minX = 0, maxX = 0, minY = 0, maxY = 0;
            for (int i = 0; i < getVertices().length; i++) {
                if (i == 0) {
                    minX = getVertices()[i].x;
                    maxX = getVertices()[i].x;
                    minY = getVertices()[i].y;
                    maxY = getVertices()[i].y;
                }
                if (getVertices()[i].x > maxX) {
                    maxX = getVertices()[i].x;
                }
                if (getVertices()[i].x < minX) {
                    minX = getVertices()[i].x;
                }
                if (getVertices()[i].y > maxY) {
                    maxY = getVertices()[i].y;
                }
                if (getVertices()[i].y < minY) {
                    minY = getVertices()[i].y;
                }
            }
            return new Rectangle(body.getPosition().x, body.getPosition().y, maxX - minX, maxY - minY);
        } else {
            float x = body.getPosition().x - body.getFixtureList().get(0).getShape().getRadius(), y = body.getPosition().y - body.getFixtureList().get(0).getShape().getRadius();
            return new Rectangle(x, y, body.getFixtureList().get(0).getShape().getRadius() * 2, body.getFixtureList().get(0).getShape().getRadius() * 2);
        }
    }

    public void DrawPolygonTexture(PolygonSpriteBatch polygonSpriteBatch, OrthographicCamera camera) {
        Rectangle roughRectangle = CalculateRoughBodyRectangle();
        polygonSpriteBatch.draw(GetPolygonRegion(), body.getPosition().x, body.getPosition().y,
                (roughRectangle.getWidth() * camera.zoom) / 2, (roughRectangle.getHeight() * camera.zoom) / 2,
                GetPolygonRegion().getRegion().getRegionWidth() * camera.zoom, GetPolygonRegion().getRegion().getRegionHeight() * camera.zoom,
                .01f / camera.zoom, .01f / camera.zoom, MathUtils.radiansToDegrees * body.getAngle());

    }
}
