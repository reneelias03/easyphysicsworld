package com.easyphysicsworldeditor.game;

import box2dLight.ConeLight;
import box2dLight.DirectionalLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Shape;
import org.w3c.dom.css.Rect;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main extends Game {
    OrthographicCamera camera;
    public static int screenWidth, screenHeight;
    float radius;
    Vector2 circPosition;
    AssetManager assetManager;
    EasyWorldEditor easyWorldEditor;
    SpriteBatch batch, staticBatch;
    RayHandler rayHandler;
    PolygonSpriteBatch polygonSpriteBatch;
    Sprite circleSprite, circManSprite, moonSprite, backgroundSprite;
    boolean cameraTrackBall, bouncePlatHit, lights, firstDominoHit, dominoSequence, backwardPlatHit;
    float backgroundX, backgroundSpeed;
    BitmapFont font;
    DecimalFormat decimalFormat;
    PointLight ballLight;
    ArrayList<ConeLight> obstBlockLights;
    //    ArrayList<Vector2>  dominoLocations;
//    ArrayList<EasyBody> dominoBodies;
    Music backgroundSong;
    EasyBody ball;
    boolean goalContact;

    @Override
    public void create() {
        Gdx.graphics.setDisplayMode(1920, 1080, true);

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        assetManager = new AssetManager();
        LoadAssets();

//        camera = new OrthographicCamera(screenWidth, screenHeight);
//        camera.position.set(screenWidth / 2, screenHeight / 2, 0);
//        camera.zoom = .01f;
//        camera.update();

        easyWorldEditor = new EasyWorldEditor(new Vector2(0, -9.8f), "Demo.xml");
        easyWorldEditor.LoadBodies(EditorWorldLoader.LoadEditorWorld("Demo.xml", easyWorldEditor.world));
        circPosition = new Vector2(easyWorldEditor.GetBody("ball").body.getPosition().x, easyWorldEditor.GetBody("ball").body.getPosition().y);
        camera = easyWorldEditor.getCamera();

//        easyWorldEditor.GetBody("BoxesRamp").SetTexture("BrushedMetal.png", assetManager, camera.zoom);
//        easyWorldEditor.GetBody("ClockSquare").SetTexture("RustyPanel80.png", assetManager, camera.zoom);
//        easyWorldEditor.GetBody("Please").SetTexture("RustyPanel80.png", assetManager, camera.zoom);
//        easyWorldEditor.GetBody("Small").SetTexture("BrushedMetal.png", assetManager, camera.zoom);
        easyWorldEditor.GetBody("ground00").SetTexture("BrushedMetal.png", assetManager, camera.zoom);

        for (EasyBody easyBody : easyWorldEditor.GetBodies().values()) {
            if (easyBody.body.getFixtureList().get(0).getShape().getType() != Shape.Type.Circle) {
                easyBody.SetTexture("BrushedMetal.png", assetManager, camera.zoom);
            }
        }

        batch = new SpriteBatch();
        staticBatch = new SpriteBatch();
        polygonSpriteBatch = new PolygonSpriteBatch();

        rayHandler = new RayHandler(easyWorldEditor.world);
        rayHandler.setAmbientLight(1, 1, 1, .075f);
//        new DirectionalLight(rayHandler, 10000, Color.WHITE, -90);
//        new PointLight(rayHandler, 750, Color.TEAL, 25f, easyWorldEditor.GetBody("greenCircle").body.getPosition().x, easyWorldEditor.GetBody("greenCircle").body.getPosition().y + 10f);
//        new PointLight(rayHandler, 750, new Color(.4f, .65f, .4f, 1f), 25f, easyWorldEditor.GetBody("greenCircle").body.getPosition().x - 20f, easyWorldEditor.GetBody("greenCircle").body.getPosition().y + 10f);
//        new ConeLight(rayHandler, 250, new Color(1,1,.2f, 1f), 20f, easyWorldEditor.GetBody("greenCircle").body.getPosition().x + 20f, easyWorldEditor.GetBody("greenCircle").body.getPosition().y + 5f, -90f, 50f);
        ballLight = new PointLight(rayHandler, 500, Color.WHITE, 1f, easyWorldEditor.GetBody("ball").body.getPosition().x, easyWorldEditor.GetBody("ball").body.getPosition().y);

        ball = easyWorldEditor.GetBody("ball");
        circleSprite = new Sprite(GetTextureAsset("SoccerballAsteroid.png"));
        backgroundSprite = new Sprite(GetTextureAsset("space.jpg"));
        backgroundSprite.setSize(screenWidth, screenHeight);
        backgroundSprite.setPosition(0, 0);

        backgroundX = 0;
        backgroundSpeed = 0;
//        circManSprite = new Sprite(GetTextureAsset("Thumbnail.png"));
//        moonSprite = new Sprite(GetTextureAsset("FullMoon.png"));
//        circPosition.x = easyWorldEditor.GetBody("greenCircle").body.getPosition().x;
//        circPosition.y =  easyWorldEditor.GetBody("greenCircle").body.getPosition().y;

//        camera.position.set(circPosition.x, circPosition.y, 0);
//        dominoLocations = new ArrayList<Vector2>();
//        dominoBodies = new ArrayList<EasyBody>();
        obstBlockLights = new ArrayList<ConeLight>();

        Rectangle roughRectangle;
        for (EasyBody easyBody : easyWorldEditor.GetBodies().values()) {
            if (easyBody.GetName().length() > 8 && easyBody.GetName().substring(0, 9).equals("obstBlock")) {
                roughRectangle = easyWorldEditor.CalculateRoughBodyRectangle(easyBody);
                obstBlockLights.add(new ConeLight(rayHandler, 250, Color.TEAL, 3f, easyBody.body.getPosition().x + roughRectangle.getWidth() / 2, easyBody.body.getPosition().y + 2f, -90f, 50f));
            }
            if (easyBody.GetName().length() > 9 && easyBody.GetName().substring(0, 10).equals("bouncePlat")) {
                roughRectangle = easyWorldEditor.CalculateRoughBodyRectangle(easyBody);
                if (easyBody.GetName().endsWith("3")) {
                    obstBlockLights.add(new ConeLight(rayHandler, 250, Color.RED, 3f, easyBody.body.getPosition().x + roughRectangle.getWidth() / 2, easyBody.body.getPosition().y + 3f, -90f, 50f));
                } else {
                    obstBlockLights.add(new ConeLight(rayHandler, 250, Color.YELLOW, 3f, easyBody.body.getPosition().x + roughRectangle.getWidth() / 2, easyBody.body.getPosition().y + 2f, -90f, 50f));
                }
            }
            if (easyBody.GetName().length() > 10 && easyBody.GetName().substring(0, 10).equals("fObstBlock")) {
                roughRectangle = easyWorldEditor.CalculateRoughBodyRectangle(easyBody);
                obstBlockLights.add(new ConeLight(rayHandler, 250, Color.MAROON, 3f, easyBody.body.getPosition().x + roughRectangle.getWidth() / 2, easyBody.body.getPosition().y + 2f, -90f, 50f));
            }
            if (easyBody.GetName().equals("goalBlock")) {
                roughRectangle = easyWorldEditor.CalculateRoughBodyRectangle(easyBody);
                obstBlockLights.add(new ConeLight(rayHandler, 250, Color.GREEN, 3f, easyBody.body.getPosition().x + roughRectangle.getWidth() / 2, easyBody.body.getPosition().y + 3f, -90f, 50f));
            }
//            if(easyBody.GetName().length() > 5 && easyBody.GetName().substring(0, 6).equals("domino")){
//                dominoLocations.add(new Vector2(easyBody.body.getPosition().x, easyBody.body.getPosition().y));
//                dominoBodies.add(easyBody);
//            }
        }

        cameraTrackBall = false;
        font = new BitmapFont();
        decimalFormat = new DecimalFormat("#.00");
        bouncePlatHit = false;
        lights = false;
        firstDominoHit = false;
        dominoSequence = false;
        goalContact = false;
        backgroundSong = Gdx.audio.newMusic(Gdx.files.internal("BackgroundMusic.mp3"));
    }

    @Override
    public void render() {

        easyWorldEditor.getCamera().update();

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        easyWorldEditor.Update();

        if (easyWorldEditor.controlsActive) {
            if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
                if (ball.body.getType() != BodyDef.BodyType.StaticBody) {
                    ball.body.setType(BodyDef.BodyType.StaticBody);
                    backgroundSong.stop();
                } else if (ball.body.getType() == BodyDef.BodyType.StaticBody && ball.body.getPosition().x != circPosition.x
                        && ball.body.getPosition().y != circPosition.y) {
                    ball.body.setTransform(circPosition, 0);
//                    for(int i = 0; i < dominoBodies.size(); i++){
//                        char lastNameChar = dominoBodies.get(i).GetName().charAt(dominoBodies.get(i).GetName().length() - 1);
//                        char intChar = Integer.toString(i).toCharArray()[0];
//                        for(int j = 0; j < dominoBodies.size(); j++) {
//                            if (dominoBodies.get(j).GetName().charAt(dominoBodies.get(i).GetName().length() - 1) == Integer.toString(i).toCharArray()[0]) {
//                                dominoBodies.get(j).body.setLinearVelocity(0, 0);
//                                dominoBodies.get(j).body.setTransform(dominoLocations.get(i).x, dominoLocations.get(i).y, 0);
//                            }
//                        }
//                    }
                    easyWorldEditor.GetBody("landingPlat0").body.getFixtureList().get(0).setFriction(0);
                    backwardPlatHit = false;
                } else {
                    ball.body.setType(BodyDef.BodyType.DynamicBody);
                    dominoSequence = false;
                    firstDominoHit = false;
                    ball.body.getFixtureList().get(0).setRestitution(.1f);
                    ball.body.getFixtureList().get(0).setDensity(.3f);
                    goalContact = false;
                    backgroundSong.play();
                }
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.B)) {
                cameraTrackBall = !cameraTrackBall;
                if (camera.zoom != .01f && cameraTrackBall) {
                    camera.zoom = .01f;
                }
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
                easyWorldEditor.shouldStep = !easyWorldEditor.shouldStep;
            } else if (Gdx.input.isKeyJustPressed(Input.Keys.L)) {
                lights = !lights;
            }
        }

        backgroundSpeed = ball.body.getLinearVelocity().x / 5f;
//        backgroundX -= backgroundSpeed;
        backgroundX = backgroundX <= -Gdx.graphics.getWidth() ? 0 : backgroundX - backgroundSpeed;
        backgroundSprite.setPosition(backgroundX, 0);

        if (cameraTrackBall) {
            camera.position.x = ball.body.getPosition().x + ball.body.getFixtureList().get(0).getShape().getRadius() / 2f;
            camera.position.y = ball.body.getPosition().y + ball.body.getFixtureList().get(0).getShape().getRadius() / 2f;
        }

//        int loopCount = 0;
//        boolean dominoContact = false;

        for (Contact contact : easyWorldEditor.world.getContactList()) {

            if (contact.getFixtureA().getBody() == ball.body || contact.getFixtureB().getBody() == ball.body) {
                if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("goalBlock").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("goalBlock").body) {
                    ball.body.setLinearVelocity(0, ball.body.getLinearVelocity().y);
                    ball.body.setAngularVelocity(0);
                    goalContact = true;
                } else if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("ground00").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("ground00").body ||
                        contact.getFixtureA().getBody() == easyWorldEditor.GetBody("landingPlat0").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("landingPlat0").body ||
                        contact.getFixtureA().getBody() == easyWorldEditor.GetBody("finalPlat").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("finalPlat").body) {
                    if (backwardPlatHit && ball.body.getAngularVelocity() < 20f && !goalContact) {
                        ball.body.applyTorque(.1f, false);
                    } else if (ball.body.getAngularVelocity() > -20f && !goalContact) {
                        ball.body.applyTorque(-.1f, false);
                        ball.body.getFixtureList().get(0).setRestitution(0f);
                    }
//                    if(contact.getFixtureA().getBody() == easyWorldEditor.GetBody("landingPlat0").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("landingPlat0").body
//                            && ball.body.getLinearVelocity().x != 17.83f) {
//                        ball.body.setLinearVelocity(17.83f,ball.body.getLinearVelocity().y);
//                    }
                    if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
                        ball.body.applyForceToCenter(0, 10f, false);
                    }
                    if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("endPipe0").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("endPipe0").body) {
                        ball.body.setLinearVelocity(ball.body.getLinearVelocity().x + 1.25f, ball.body.getLinearVelocity().y + 1.25f);
                    }


//                    if(contact.getFixtureA().getBody() == easyWorldEditor.GetBody("endPipe0").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("endPipe0").body){
//                        ball.body.
//                    }
                }
                if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("bouncePlat3").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("bouncePlat3").body) {
                    backwardPlatHit = true;
                }
                if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("bouncePlat2").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("bouncePlat2").body) {
                    ball.body.getFixtureList().get(0).setRestitution(.1f);
                }
                if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("bouncePlat").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("bouncePlat").body) {
                    ball.body.getFixtureList().get(0).setRestitution(0f);
                }
                if (!bouncePlatHit) {
                    if (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("bouncePlat").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("bouncePlat").body) {
                        bouncePlatHit = true;
//                        break;
                    }
                }
//                if(!dominoSequence && !firstDominoHit && contact.getFixtureA().getBody() == easyWorldEditor.GetBody("domino0").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("domino0").body) {
//                    firstDominoHit = true;
//                    break;
//                }
//                if(firstDominoHit && (contact.getFixtureA().getBody() == easyWorldEditor.GetBody("domino0").body || contact.getFixtureB().getBody() == easyWorldEditor.GetBody("domino0").body)) {
//                    dominoContact = true;
//                }
//                if(firstDominoHit && !dominoContact){
//                    firstDominoHit = false;
//                    dominoSequence = true;
//                    for(EasyBody dominoBody : dominoBodies) {
//                        dominoBody.body.getFixtureList().get(0).setFriction(1f);
//                    }
//                    loopCount++;
//                ball.body.setTransform(ball.body.getPosition().x - 1f, ball.body.getPosition().y, ball.body.getAngle());
//                }
            }
        }
        if (bouncePlatHit && easyWorldEditor.world.getContactList().size == 0) {
            bouncePlatHit = false;
            if (ball.body.getLinearVelocity().x != 17.5f) {
                ball.body.setLinearVelocity(17.5f, -1.2f);
            }
        }
//        if(!dominoSequence && ball.body.getPosition().x > easyWorldEditor.GetBody("obstBlock9").body.getPosition().x && ball.body.getFixtureList().get(0).getRestitution() == 0f) {
//            ball.body.getFixtureList().get(0).setRestitution(.1f);
//            ball.body.getFixtureList().get(0).setDensity(.5f);
//        }

//        if(dominoSequence) {
//            ball.body.setLinearVelocity(0, ball.body.getLinearVelocity().y);
//            for(EasyBody dominoBody : dominoBodies) {
//                dominoBody.body.setLinearVelocity(0, dominoBody.body.getLinearVelocity().y);
//            }
//            ball.body.setAngularVelocity(0);
//            ball.body.setLinearVelocity(0,ball.body.getLinearVelocity().y);
//        }

        if (lights) {
            ballLight.setPosition(ball.body.getPosition().x, ball.body.getPosition().y);
        }

        circleSprite.setSize(ball.body.getFixtureList().get(0).getShape().getRadius() * 2f, ball.body.getFixtureList().get(0).getShape().getRadius() * 2f);
        circleSprite.setOriginCenter();
        circleSprite.setPosition(ball.body.getPosition().x - circleSprite.getOriginX(), ball.body.getPosition().y - circleSprite.getOriginY());
        circleSprite.setRotation(ball.body.getAngle() * MathUtils.radiansToDegrees);

        //region CircleUpdate


//        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
//            easyWorldEditor.GetBody("greenCircle").body.applyLinearImpulse(0, .5f, circPosition.x, circPosition.y, true);
//        }
//        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
//            easyWorldEditor.GetBody("greenCircle").body.setTransform(screenWidth / 2, 103, 0f);
//            easyWorldEditor.GetBody("greenCircle").body.setLinearVelocity(0, 0);
//
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
//            radius += .01;
//            easyWorldEditor.GetBody("greenCircle").body.getFixtureList().get(0).getShape().setRadius(radius);
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
//            radius -= .01;
//            easyWorldEditor.GetBody("greenCircle").body.getFixtureList().get(0).getShape().setRadius(radius);
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
//            easyWorldEditor.GetBody("greenCircle").body.applyTorque(-.5f, true);
//        }
//        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
//            easyWorldEditor.GetBody("greenCircle").body.applyTorque(.5f, true);
//        }
//
//        easyWorldEditor.GetBody("greenCircle").body.setAngularDamping(1f);

//
//        circManSprite.setSize(easyWorldEditor.GetBody("circMan").body.getFixtureList().get(0).getShape().getRadius() * 2f, easyWorldEditor.GetBody("circMan").body.getFixtureList().get(0).getShape().getRadius() * 2f);
//        circManSprite.setOriginCenter();
//        circManSprite.setPosition(easyWorldEditor.GetBody("circMan").body.getPosition().x - circManSprite.getOriginX(), easyWorldEditor.GetBody("circMan").body.getPosition().y - circManSprite.getOriginY());
//        circManSprite.setRotation(easyWorldEditor.GetBody("circMan").body.getAngle() * MathUtils.radiansToDegrees);
//
//        moonSprite.setSize(easyWorldEditor.GetBody("Moon").body.getFixtureList().get(0).getShape().getRadius() * 2f, easyWorldEditor.GetBody("Moon").body.getFixtureList().get(0).getShape().getRadius() * 2f);
//        moonSprite.setOriginCenter();
//        moonSprite.setPosition(easyWorldEditor.GetBody("Moon").body.getPosition().x - moonSprite.getOriginX(), easyWorldEditor.GetBody("Moon").body.getPosition().y - moonSprite.getOriginY());
//        moonSprite.setRotation(easyWorldEditor.GetBody("Moon").body.getAngle() * MathUtils.radiansToDegrees);
        //endregion


        Draw();
    }

    private void Draw() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
//        Gdx.gl.glClearColor(0, .1f, .6f, 1);
        staticBatch.begin();
        backgroundSprite.draw(staticBatch);
        backgroundSprite.setPosition(backgroundX + Gdx.graphics.getWidth(), 0);
        backgroundSprite.draw(staticBatch);
        backgroundSprite.setPosition(backgroundX, 0);
        staticBatch.end();
        polygonSpriteBatch.setProjectionMatrix(camera.combined);
        polygonSpriteBatch.begin();
        for (EasyBody easyBody : easyWorldEditor.GetBodies().values()) {
            if (easyBody.GetPolygonRegion() != null) {
                Rectangle roughRectangle = easyWorldEditor.CalculateRoughBodyRectangle(easyBody);
                polygonSpriteBatch.draw(easyBody.GetPolygonRegion(), easyBody.body.getPosition().x, easyBody.body.getPosition().y,
                        (roughRectangle.getWidth() * camera.zoom) / 2, (roughRectangle.getHeight() * camera.zoom) / 2,
                        easyBody.GetPolygonRegion().getRegion().getRegionWidth() * camera.zoom, easyBody.GetPolygonRegion().getRegion().getRegionHeight() * camera.zoom,
                        .01f / camera.zoom, .01f / camera.zoom, MathUtils.radiansToDegrees * easyBody.body.getAngle());
            }
        }

        polygonSpriteBatch.end();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        circleSprite.draw(batch);
//        circManSprite.draw(batch);
//        moonSprite.draw(batch);
        batch.end();
        if (lights) {
            rayHandler.setCombinedMatrix(camera.combined);
            rayHandler.updateAndRender();
        }
        staticBatch.begin();
        font.draw(staticBatch, String.format("xSpeed: " + decimalFormat.format(ball.body.getLinearVelocity().x) + ", ySpeed: " + decimalFormat.format(ball.body.getLinearVelocity().y)), Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight());
        font.draw(staticBatch, String.format("Angular Velocity: " + decimalFormat.format(ball.body.getAngularVelocity())), Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - font.getLineHeight());
        staticBatch.end();

        easyWorldEditor.Draw(rayHandler);
    }

    private void LoadAssets() {
        assetManager.load("Bamboo.jpg", Texture.class);
        assetManager.load("EditorButton.png", Texture.class);
        assetManager.load("SavePolygonButton.png", Texture.class);
        assetManager.load("EmptyCircle.png", Texture.class);
        assetManager.load("BrickWall.jpg", Texture.class);
        assetManager.load("WhitePixel.png", Texture.class);
        assetManager.load("ShowGridButton.png", Texture.class);
        assetManager.load("ExportWorldButton.png", Texture.class);
        assetManager.load("BrushedMetal.png", Texture.class);
        assetManager.load("RustyPanel80.png", Texture.class);
        assetManager.load("SoccerballAsteroid.png", Texture.class);
        assetManager.load("FullMoon.png", Texture.class);
        assetManager.load("Thumbnail.png", Texture.class);
        assetManager.load("space.jpg", Texture.class);
        assetManager.finishLoading();
    }

    public Texture GetTextureAsset(String fileName) {
        return assetManager.get(fileName, Texture.class);
    }


}
