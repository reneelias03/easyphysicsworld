package com.easyphysicsworldeditor.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Shape;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ReneElias on 4/1/2015.
 */
 class PolygonEditorDialog extends Sprite {
    AssetManager assetManager;
    HashMap<String, GameButtons> dialogButtons;
    HashMap<String, Sprite> dialogSprites;
    Rectangle nameHitbox;
    float scale, cursorFlashTime, elapsedCursorTime, cursorXLocation, backspacePressedTimeElapsed, backspacePressedTimeTarget;
    BitmapFont ubuntuFont, ubuntuFontLight;
    boolean awake, active, nameTapped, showCursor, frictionTextTapped, densityTextTapped, restitutionTextTapped, created, exit, backspacePressed;
    public boolean GetExit() {
        return exit;
    }
    public void ResetExit() {
        exit = false;
    }
    public boolean Created() {
        return created;
    }
    boolean[] bodyButtonBooleans;
    String name, frictionValueString, densityValueString, restitutionValueString;
    KeyboardManager keyboardManager;

    public PolygonEditorDialog(Texture texture, AssetManager assetManager){
        super(texture);
        scale = (Gdx.graphics.getWidth() * .625f) / getWidth();
        setSize(getWidth() * scale, getHeight() * scale);
        setPosition(Gdx.graphics.getWidth() * .1875f, Gdx.graphics.getHeight() * .1875f);
        this.assetManager = assetManager;
        Texture spriteSheet = GetTextureAsset("SpriteSheet.png");
        dialogButtons = new HashMap<String, GameButtons>();
        dialogButtons.put("DynamicButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + 60f * scale, getY() + getHeight() - 204f * scale)));
        dialogButtons.get("DynamicButton").setRegion(210, 105, 100, 32);
        dialogButtons.get("DynamicButton").setSize(100f * scale, 32f * scale);
        dialogButtons.put("KinematicButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + 170f * scale, getY() + getHeight() - 204f * scale)));
        dialogButtons.get("KinematicButton").setRegion(310, 105, 100, 32);
        dialogButtons.get("KinematicButton").setSize(100f * scale, 32f * scale);
        dialogButtons.put("StaticButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + 280f * scale, getY() + getHeight() - 204f * scale)));
        dialogButtons.get("StaticButton").setRegion(410, 105, 100, 32);
        dialogButtons.get("StaticButton").setSize(100f * scale, 32f * scale);
        dialogButtons.put("AwakeButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + 170f * scale, getY() + getHeight() - 477f * scale)));
        dialogButtons.get("AwakeButton").setRegion(210, 233, 80, 32);
        dialogButtons.get("AwakeButton").setSize(80f * scale, 32f * scale);
        dialogButtons.put("ActiveButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + 390f * scale, getY() + getHeight() - 477f * scale)));
        dialogButtons.get("ActiveButton").setRegion(210, 233, 80, 32);
        dialogButtons.get("ActiveButton").setSize(80f * scale, 32f * scale);
        dialogButtons.put("ExitButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + 920f * scale, getY() + getHeight() - 80f * scale)));
        dialogButtons.get("ExitButton").setRegion(310, 201, 80, 80);
        dialogButtons.get("ExitButton").setSize(80f * scale, 80f * scale);
        dialogButtons.put("CreateButton", new GameButtons(new TextureRegion(spriteSheet), true, new Vector2(getX() + getWidth() - 190f * scale, getY() + getHeight() - 522f * scale)));
        dialogButtons.get("CreateButton").setRegion(510, 105, 130, 32);
        dialogButtons.get("CreateButton").setSize(130f * scale, 32f * scale);
        dialogButtons.put("FrictionTextBox", new GameButtons(GetTextureAsset("TextBox.png"), true, new Vector2(getX() + 60f * scale, getY() + getHeight() - 388f * scale)));
        dialogButtons.get("FrictionTextBox").setSize(210f * scale, 80f * scale);
        dialogButtons.put("DensityTextBox", new GameButtons(GetTextureAsset("TextBox.png"), true, new Vector2(getX() + 280f * scale, getY() + getHeight() - 388f * scale)));
        dialogButtons.get("DensityTextBox").setSize(210f * scale, 80f * scale);
        dialogButtons.put("RestitutionTextBox", new GameButtons(GetTextureAsset("TextBox.png"), true, new Vector2(getX() + 500f * scale, getY() + getHeight() - 388f * scale)));
        dialogButtons.get("RestitutionTextBox").setSize(210f * scale, 80f * scale);

        dialogSprites = new HashMap<String, Sprite>();
        dialogSprites.put("FrictionSprite", new Sprite(GetTextureAsset("IconFriction.png")));
        dialogSprites.get("FrictionSprite").setSize(52f * scale, 18f * scale);
        dialogSprites.get("FrictionSprite").setPosition(getX() + 60f * scale, getY() + getHeight() - 300f * scale + 18f * scale);
        dialogSprites.put("DensitySprite", new Sprite(GetTextureAsset("IconDensity.png")));
        dialogSprites.get("DensitySprite").setSize(50f * scale, 26f * scale);
        dialogSprites.get("DensitySprite").setPosition(getX() + 280f * scale, getY() + getHeight() - 300f * scale + 18f * scale);
        dialogSprites.put("RestitutionSprite", new Sprite(GetTextureAsset("IconRestitution.png")));
        dialogSprites.get("RestitutionSprite").setSize(50f * scale, 25f * scale);
        dialogSprites.get("RestitutionSprite").setPosition(getX() + 500f * scale, getY() + getHeight() - 300f * scale + 18f * scale);
        dialogSprites.put("BodySprite", new Sprite(GetTextureAsset("IconBody.png")));
        dialogSprites.get("BodySprite").setSize(17f * scale, 17f * scale);
        dialogSprites.get("BodySprite").setPosition(getX() + 60f * scale, getY() + getHeight() - 168f * scale + 17f * scale);
        dialogSprites.put("LineBreak", new Sprite(GetTextureAsset("LineBreak.png")));
        dialogSprites.get("LineBreak").setSize(getWidth(), dialogSprites.get("LineBreak").getHeight() * (getWidth() / dialogSprites.get("LineBreak").getWidth()));
        dialogSprites.put("NameLine", new Sprite(GetTextureAsset("NameLine.png")));
        dialogSprites.get("NameLine").setSize(430f * scale, 3f * scale);
        dialogSprites.get("NameLine").setPosition(getX() + 60f * scale, getY() + getHeight() - 106f * scale);

        nameHitbox = new Rectangle(dialogSprites.get("NameLine").getX(), dialogSprites.get("NameLine").getY(), dialogSprites.get("NameLine").getWidth(), 60f * scale);

        ubuntuFont = new BitmapFont(Gdx.files.local("UbuntuMedium.fnt"), new TextureRegion(GetTextureAsset("UbuntuMedium.png")), false);
        ubuntuFontLight = new BitmapFont(Gdx.files.local("UbuntuLight.fnt"), new TextureRegion(GetTextureAsset("UbuntuLight.png")), false);
        awake = true;
        active = true;
        nameTapped = false;
        exit = false;

        bodyButtonBooleans = new boolean[3];
        for(boolean activeBool : bodyButtonBooleans) {
            activeBool = false;
        }

        name = "";
        keyboardManager = new KeyboardManager();

        cursorFlashTime = .75f;
        elapsedCursorTime = 0;
        showCursor = true;
        cursorXLocation = dialogSprites.get("NameLine").getX();

        frictionValueString = "0.2";
        densityValueString = "1.0";
        restitutionValueString = "0.0";

        frictionTextTapped = false;
        densityTextTapped  = false;
        restitutionTextTapped = false;

        backspacePressed = false;
        backspacePressedTimeElapsed = 0;
        backspacePressedTimeTarget = .5f;
    }

    public Texture GetTextureAsset(String fileName) {
        return assetManager.get(fileName, Texture.class);
    }

    public void Update() {
        for(GameButtons button : dialogButtons.values()) {
            button.Update();
        }
        if(dialogButtons.get("AwakeButton").Tapped()) {
           awake = !awake;
            if(awake) {
                dialogButtons.get("AwakeButton").setRegion(210, 233, 80, 32);
            } else {
                dialogButtons.get("AwakeButton").setRegion(210, 201, 80, 32);
            }
        }
        if(dialogButtons.get("ActiveButton").Tapped()) {
            active = !active;
            if(active) {
                dialogButtons.get("ActiveButton").setRegion(210, 233, 80, 32);
            } else {
                dialogButtons.get("ActiveButton").setRegion(210, 201, 80, 32);
            }
        }
        if(dialogButtons.get("DynamicButton").Tapped()) {
            for(int i = 0; i < bodyButtonBooleans.length; i++) {
                if(i == 0) {
                    bodyButtonBooleans[i] = true;
                } else {
                    bodyButtonBooleans[i] = false;
                }
            }
            dialogButtons.get("DynamicButton").setRegion(210, 169, 100, 32);
            dialogButtons.get("KinematicButton").setRegion(310, 105, 100, 32);
            dialogButtons.get("StaticButton").setRegion(410, 105, 100, 32);
        }
        if(dialogButtons.get("KinematicButton").Tapped()) {
            for(int i = 0; i < bodyButtonBooleans.length; i++) {
                if(i == 1) {
                    bodyButtonBooleans[i] = true;
                } else {
                    bodyButtonBooleans[i] = false;
                }
            }
            dialogButtons.get("DynamicButton").setRegion(210, 105, 100, 32);
            dialogButtons.get("KinematicButton").setRegion(310, 169, 100, 32);
            dialogButtons.get("StaticButton").setRegion(410, 105, 100, 32);
        }
        if(dialogButtons.get("StaticButton").Tapped()) {
            for(int i = 0; i < bodyButtonBooleans.length; i++) {
                if(i == 2) {
                    bodyButtonBooleans[i] = true;
                } else {
                    bodyButtonBooleans[i] = false;
                }
            }
            dialogButtons.get("DynamicButton").setRegion(210, 105, 100, 32);
            dialogButtons.get("KinematicButton").setRegion(310, 105, 100, 32);
            dialogButtons.get("StaticButton").setRegion(410, 169, 100, 32);
        }
        if(dialogButtons.get("FrictionTextBox").Tapped() && !frictionTextTapped) {
            frictionTextTapped = true;
            densityTextTapped = false;
            restitutionTextTapped = false;
            nameTapped = false;
        } else if(dialogButtons.get("DensityTextBox").Tapped() && !densityTextTapped) {
            frictionTextTapped = false;
            densityTextTapped = true;
            restitutionTextTapped = false;
            nameTapped = false;
        } else if(dialogButtons.get("RestitutionTextBox").Tapped() && !restitutionTextTapped) {
            frictionTextTapped = false;
            densityTextTapped = false;
            restitutionTextTapped = true;
            nameTapped = false;
        }
        if(Gdx.input.isButtonPressed(Input.Buttons.LEFT) && GestureManager.IsCurrentlyBeingTouched(nameHitbox)) {
            nameTapped = true;
            frictionTextTapped = false;
            densityTextTapped = false;
            restitutionTextTapped = false;
        } else if(Gdx.input.isButtonPressed(Input.Buttons.LEFT) && !GestureManager.IsCurrentlyBeingTouched(nameHitbox) && name == "") {
            nameTapped = false;
        }

        if(dialogButtons.get("ExitButton").Tapped()) {
            ResetValues();
            exit = true;
        }

        if(dialogButtons.get("CreateButton").Tapped() || Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            CreatePolygon();
        }

        KeyInput();

        elapsedCursorTime += Gdx.graphics.getDeltaTime();
        if(elapsedCursorTime > cursorFlashTime) {
            showCursor = !showCursor;
            elapsedCursorTime = 0;
        }
    }

    public void Draw(SpriteBatch batch) {
        draw(batch);
        for(GameButtons button : dialogButtons.values()) {
            button.draw(batch);
        }
        for(Sprite sprite : dialogSprites.values()) {
            if(sprite != dialogSprites.get("LineBreak")) {
                sprite.draw(batch);
            }
        }
        dialogSprites.get("LineBreak").setPosition(getX(), getY() + getHeight() - 224f * scale);
        dialogSprites.get("LineBreak").draw(batch);
        dialogSprites.get("LineBreak").setPosition(getX(), getY() + getHeight() - 408f * scale);
        dialogSprites.get("LineBreak").draw(batch);

        TextDraw(batch);
    }

    private void TextDraw(SpriteBatch batch) {
        ubuntuFont.setScale(.375f * scale);
        ubuntuFont.setColor(.65f, .66f, .67f, 1f);
        ubuntuFont.draw(batch, "Friction", dialogSprites.get("FrictionSprite").getX() + dialogSprites.get("FrictionSprite").getWidth() + 10f * scale, dialogSprites.get("FrictionSprite").getY() + ubuntuFont.getLineHeight() * .65f);
        ubuntuFont.draw(batch, "Density", dialogSprites.get("DensitySprite").getX() + dialogSprites.get("DensitySprite").getWidth() + 10f * scale, dialogSprites.get("DensitySprite").getY() + ubuntuFont.getLineHeight() * .65f);
        ubuntuFont.draw(batch, "Restitution", dialogSprites.get("RestitutionSprite").getX() + dialogSprites.get("RestitutionSprite").getWidth() + 10f * scale, dialogSprites.get("RestitutionSprite").getY() + ubuntuFont.getLineHeight() * .65f);
        ubuntuFont.draw(batch, "Awake", getX() + 60f * scale, dialogButtons.get("AwakeButton").getY() + ubuntuFont.getLineHeight() * 1.15f);
        ubuntuFont.draw(batch, "Active", getX() + 290f * scale, dialogButtons.get("AwakeButton").getY() + ubuntuFont.getLineHeight() * 1.15f);
        ubuntuFont.draw(batch, "Body Type", getX() + 85f * scale, dialogSprites.get("BodySprite").getY() + ubuntuFont.getLineHeight() * .75f);
        ubuntuFontLight.setColor(.819f, .827f, .831f, 1f);
        ubuntuFontLight.setScale(.83f * scale);
        if(!nameTapped && name == "") {
            ubuntuFontLight.draw(batch, "Name", dialogSprites.get("NameLine").getX(), dialogSprites.get("NameLine").getY() + ubuntuFontLight.getLineHeight() * .75f);
        } else {
            if(showCursor && nameTapped) {
                ubuntuFontLight.setColor(.719f, .727f, .731f, 1f);
                ubuntuFontLight.draw(batch, "|", dialogSprites.get("NameLine").getX() + ubuntuFontLight.getBounds(name).width, dialogSprites.get("NameLine").getY() + ubuntuFontLight.getLineHeight() * .82f);
            }
            ubuntuFontLight.setColor(.553f, .776f, .247f, 1f);
            ubuntuFontLight.draw(batch, name, dialogSprites.get("NameLine").getX(), dialogSprites.get("NameLine").getY() + ubuntuFontLight.getLineHeight() * .75f);
        }
        ubuntuFontLight.setColor(.553f, .776f, .247f, 1f);
        ubuntuFontLight.setScale(1f * scale);
        ubuntuFontLight.draw(batch, frictionValueString, dialogButtons.get("FrictionTextBox").getX() + 10f * scale, dialogButtons.get("FrictionTextBox").getY() + ubuntuFontLight.getLineHeight() * .9f);
        if(frictionTextTapped) {
            if(showCursor) {
                ubuntuFontLight.setColor(.719f, .727f, .731f, 1f);
                ubuntuFontLight.draw(batch, "|", dialogButtons.get("FrictionTextBox").getX() + 10f * scale + ubuntuFontLight.getBounds(frictionValueString).width, dialogButtons.get("FrictionTextBox").getY() + ubuntuFontLight.getLineHeight() * .925f);
                ubuntuFontLight.setColor(.553f, .776f, .247f, 1f);
            }
        }
        ubuntuFontLight.draw(batch, densityValueString, dialogButtons.get("DensityTextBox").getX() + 10f * scale, dialogButtons.get("DensityTextBox").getY() + ubuntuFontLight.getLineHeight() * .9f);
        if(densityTextTapped) {
            if(showCursor) {
                ubuntuFontLight.setColor(.719f, .727f, .731f, 1f);
                ubuntuFontLight.draw(batch, "|", dialogButtons.get("DensityTextBox").getX() + 10f * scale + ubuntuFontLight.getBounds(densityValueString).width, dialogButtons.get("DensityTextBox").getY() + ubuntuFontLight.getLineHeight() * .925f);
                ubuntuFontLight.setColor(.553f, .776f, .247f, 1f);
            }
        }
        ubuntuFontLight.draw(batch, restitutionValueString, dialogButtons.get("RestitutionTextBox").getX() + 10f * scale, dialogButtons.get("RestitutionTextBox").getY() + ubuntuFontLight.getLineHeight() * .9f);
        if(restitutionTextTapped) {
            if(showCursor) {
                ubuntuFontLight.setColor(.719f, .727f, .731f, 1f);
                ubuntuFontLight.draw(batch, "|", dialogButtons.get("RestitutionTextBox").getX() + 10f * scale + ubuntuFontLight.getBounds(restitutionValueString).width, dialogButtons.get("RestitutionTextBox").getY() + ubuntuFontLight.getLineHeight() * .925f);
                ubuntuFontLight.setColor(.553f, .776f, .247f, 1f);
            }
        }
    }

    private void CreatePolygon(){
        created = true;
        if(name == "") {
            created = false;
        }
        for(int i = 0; i < bodyButtonBooleans.length; i++) {
            if(bodyButtonBooleans[i] == true) {
                break;
            } else if (i == bodyButtonBooleans.length - 1) {
                created = false;
            }
        }
        if(frictionValueString == "" || frictionValueString == ".") {
            created = false;
        }
        if(densityValueString == "" || densityValueString == ".") {
            created = false;
        }
        if(restitutionValueString == "" || restitutionValueString == ".") {
            created = false;
        }
    }

    private void KeyInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.ANY_KEY))
        {
            if(nameTapped) {
                if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
                    name += " ";
                    backspacePressed = false;
                } else if (Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE) && name != "") {
                    backspacePressed = true;
                    name = name.substring(0, name.length() - 1);
                    if (name.length() == 0) {
                        name = "";
                    }
                } else {
                    char charToAdd = keyboardManager.GetTypedKey();
                    if (charToAdd != '\0') {
                        name += charToAdd;
                    }
                    backspacePressed = false;
                }
            } else if(frictionTextTapped) {
                if (Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE) && frictionValueString != "") {
                    backspacePressed = true;
                    frictionValueString = frictionValueString.substring(0, frictionValueString.length() - 1);
                    if (frictionValueString.length() == 0) {
                        frictionValueString = "";
                    }
                } else {
                    char charToAdd = keyboardManager.GetTypedNumberKey();
                    if (charToAdd != '\0' && frictionValueString.length() < 5) {
                        boolean toAdd = true;
                        if(charToAdd == '.') {
                          for(int i = 0; i < frictionValueString.length(); i++) {
                              if(frictionValueString.toCharArray()[i] == '.') {
                                  toAdd = false;
                              }
                          }
                        }
                        if(toAdd) {
                            frictionValueString += charToAdd;
                        }
                    }
                    backspacePressed = false;
                }
            } else if(densityTextTapped) {
                if (Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE) && densityValueString != "") {
                    backspacePressed = true;
                    densityValueString = densityValueString.substring(0, densityValueString.length() - 1);
                    if (densityValueString.length() == 0) {
                        densityValueString = "";
                    }
                } else {
                    char charToAdd = keyboardManager.GetTypedNumberKey();

                    if (charToAdd != '\0' && densityValueString.length() < 5) {
                        boolean toAdd = true;
                        if (charToAdd == '.') {
                            for (int i = 0; i < densityValueString.length(); i++) {
                                if (densityValueString.toCharArray()[i] == '.') {
                                    toAdd = false;
                                }
                            }
                        }
                        if (toAdd) {
                            densityValueString += charToAdd;
                        }
                    }
                    backspacePressed = false;
                }
            } else if(restitutionTextTapped) {
                if (Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE) && restitutionValueString != "") {
                    backspacePressed = true;
                    restitutionValueString = restitutionValueString.substring(0, restitutionValueString.length() - 1);
                    if (restitutionValueString.length() == 0) {
                        restitutionValueString = "";
                    }
                } else {
                    char charToAdd = keyboardManager.GetTypedNumberKey();
                    if (charToAdd != '\0' && restitutionValueString.length() < 5) {
                        boolean toAdd = true;
                        if (charToAdd == '.') {
                            for (int i = 0; i < restitutionValueString.length(); i++) {
                                if (restitutionValueString.toCharArray()[i] == '.') {
                                    toAdd = false;
                                }
                            }
                        }
                        if(toAdd) {
                            restitutionValueString += charToAdd;
                        }
                    }
                    backspacePressed = false;
                }
            }
        }
    }

    public String[] GetPolygonInfo() {
        String[] polygonInfo = new String[7];
        polygonInfo[0] = name;
        if(bodyButtonBooleans[0]) {
            polygonInfo[1] = "Dynamic";
        } else if(bodyButtonBooleans[1]) {
            polygonInfo[1] = "Kinematic";
        } else {
            polygonInfo[1] = "Static";
        }
        polygonInfo[2] = frictionValueString;
        polygonInfo[3] = densityValueString;
        polygonInfo[4] = restitutionValueString;
        if(awake) {
            polygonInfo[5] = "true";
        } else {
            polygonInfo[5] = "false";
        }
        if(active) {
            polygonInfo[6] = "true";
        } else {
            polygonInfo[6] = "false";
        }
        created = false;
        return polygonInfo;
    }

    public void ResetValues() {
        name = "";
        frictionValueString = "0.2";
        densityValueString = "1.0";
        restitutionValueString = "0.0";

        frictionTextTapped = false;
        densityTextTapped  = false;
        restitutionTextTapped = false;
        nameTapped = false;
        awake = true;
        active = true;

        dialogButtons.get("DynamicButton").setRegion(210, 105, 100, 32);
        dialogButtons.get("KinematicButton").setRegion(310, 105, 100, 32);
        dialogButtons.get("StaticButton").setRegion(410, 105, 100, 32);

        dialogButtons.get("AwakeButton").setRegion(210, 233, 80, 32);
        dialogButtons.get("ActiveButton").setRegion(210, 233, 80, 32);



        for(boolean activeBool : bodyButtonBooleans) {
            activeBool = false;
        }
    }

    public void CopyValues(EasyBody easyBody, boolean editing) {
        if(editing){
            name = easyBody.GetName();
        }
        if(easyBody.body.getFixtureList().get(0).getShape().getType() == Shape.Type.Polygon) {
            frictionValueString = String.valueOf(easyBody.body.getFixtureList().get(0).getFriction());
        } else {
            frictionValueString = String.valueOf(easyBody.body.getAngularDamping());
        }
        densityValueString = String.valueOf(easyBody.body.getFixtureList().get(0).getDensity());
        restitutionValueString = String.valueOf(easyBody.body.getFixtureList().get(0).getRestitution());

        if(easyBody.body.getType() == BodyDef.BodyType.DynamicBody) {
            for(int i = 0; i < bodyButtonBooleans.length; i++) {
                if(i == 0) {
                    bodyButtonBooleans[i] = true;
                } else {
                    bodyButtonBooleans[i] = false;
                }
            }
            dialogButtons.get("DynamicButton").setRegion(210, 169, 100, 32);
            dialogButtons.get("KinematicButton").setRegion(310, 105, 100, 32);
            dialogButtons.get("StaticButton").setRegion(410, 105, 100, 32);
        } else if(easyBody.body.getType() == BodyDef.BodyType.KinematicBody) {
            for(int i = 0; i < bodyButtonBooleans.length; i++) {
                if(i == 1) {
                    bodyButtonBooleans[i] = true;
                } else {
                    bodyButtonBooleans[i] = false;
                }
            }
            dialogButtons.get("DynamicButton").setRegion(210, 105, 100, 32);
            dialogButtons.get("KinematicButton").setRegion(310, 169, 100, 32);
            dialogButtons.get("StaticButton").setRegion(410, 105, 100, 32);
        } else {
            for(int i = 0; i < bodyButtonBooleans.length; i++) {
                if(i == 2) {
                    bodyButtonBooleans[i] = true;
                } else {
                    bodyButtonBooleans[i] = false;
                }
            }
            dialogButtons.get("DynamicButton").setRegion(210, 105, 100, 32);
            dialogButtons.get("KinematicButton").setRegion(310, 105, 100, 32);
            dialogButtons.get("StaticButton").setRegion(410, 169, 100, 32);
        }
    }

}
