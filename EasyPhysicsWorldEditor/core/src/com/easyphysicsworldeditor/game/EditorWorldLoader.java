package com.easyphysicsworldeditor.game;

import com.badlogic.gdx.physics.box2d.World;

import java.util.HashMap;

/**
 * Created by rene__000 on 3/27/2015.
 */
public class EditorWorldLoader extends EasyWorldLoader {
    public static HashMap<String, EasyBody> LoadEditorWorld(String xmlFile, World world){
        LoadWorld(xmlFile, world);
        return easyBodyMap;
    }
}
