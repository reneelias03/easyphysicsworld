package com.easyphysicsworldeditor.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by rene__000 on 3/27/2015.
 */
public class EasyWorldLoader {
    static World world;
    protected static HashMap<String, EasyBody> easyBodyMap;

    public static void LoadWorld(String xmlFile) {
        LoadWorld(xmlFile, null);
    }

    public static void LoadWorld(String xmlFile, World tempWorld) {
        FileHandle file = new FileHandle(xmlFile);
        XmlReader xmlReader = new XmlReader();
        HashMap<String, EasyBody> tempBodyMap = new HashMap<String, EasyBody>();
        if(tempWorld == null) {
            world = new World(new Vector2(0, -9.8f), false);
        } else {
            world = tempWorld;
        }

        try {
            XmlReader.Element xmlParent = xmlReader.parse(file.reader());
            for(XmlReader.Element body : xmlParent.getChildrenByName("Body")) {
                if(body.getChildByName("Shape").getText() != null && body.getChildByName("Shape").getText().equals("Circle")) {
                    BodyDef circleBodyDef = new BodyDef();
                    circleBodyDef.position.x = Float.parseFloat(body.getChildByName("X").getText());
                    circleBodyDef.position.y = Float.parseFloat(body.getChildByName("Y").getText());
                    circleBodyDef.type = BodyDef.BodyType.valueOf(body.getChildByName("BodyType").getText());
                    circleBodyDef.allowSleep = false;
                    circleBodyDef.angle = Float.parseFloat(body.getChildByName("Angle").getText());

                    CircleShape circleShape = new CircleShape();
                    circleShape.setRadius(Float.parseFloat(body.getChildByName("Radius").getText()));

                    FixtureDef circleDef = new FixtureDef();
                    circleDef.restitution = Float.parseFloat(body.getChildByName("Restitution").getText());
                    circleDef.shape = circleShape;
                    circleDef.density = Float.parseFloat(body.getChildByName("Density").getText());

                    tempBodyMap.put(body.getChildByName("Name").getText(), new EasyBody(world, circleBodyDef, circleDef, body.getChildByName("Name").getText()));
                    tempBodyMap.get(body.getChildByName("Name").getText()).body.setAngularDamping(Float.parseFloat(body.getChildByName("AngularDamping").getText()));
                    circleShape.dispose();
                }
                else if(body.getChildByName("Shape").getText() != null){
                    BodyDef polyBodyDef = new BodyDef();
                    polyBodyDef.position.x = Float.parseFloat(body.getChildByName("X").getText());
                    polyBodyDef.position.y = Float.parseFloat(body.getChildByName("Y").getText());
                    polyBodyDef.type = BodyDef.BodyType.valueOf(body.getChildByName("BodyType").getText());
                    polyBodyDef.angle = Float.parseFloat(body.getChildByName("Angle").getText());

                    XmlReader.Element verticesElement = body.getChildByName("Vertices");
                    Vector2[] vertices = new Vector2[verticesElement.getChildrenByName("Vertex").size];
                    int i = 0;
                    for(XmlReader.Element vertex : verticesElement.getChildrenByName("Vertex"))
                    {
                        vertices[i] = new Vector2(Float.parseFloat(vertex.getAttribute("X")), Float.parseFloat(vertex.getAttribute("Y")));
                        i++;
                    }
                    PolygonShape polyBox = new PolygonShape();
                    polyBox.set(vertices);

                    FixtureDef groundDef = new FixtureDef();
                    groundDef.density = Float.parseFloat(body.getChildByName("Density").getText());
                    groundDef.shape = polyBox;
                    groundDef.friction = Float.parseFloat(body.getChildByName("Friction").getText());
                    groundDef.restitution = Float.parseFloat(body.getChildByName("Restitution").getText());
                    tempBodyMap.put(body.getChildByName("Name").getText(), new EasyBody(world, polyBodyDef, groundDef, body.getChildByName("Name").getText()));
                    tempBodyMap.get(body.getChildByName("Name").getText()).setVertices(vertices);
                    polyBox.dispose();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        easyBodyMap = tempBodyMap;
        world = null;
    }

    public static EasyBody GetBody(String bodyName) {
        return easyBodyMap.get(bodyName);
    }
}
