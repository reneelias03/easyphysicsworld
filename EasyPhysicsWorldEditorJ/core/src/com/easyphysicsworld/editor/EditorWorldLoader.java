package com.easyphysicsworld.editor;

import com.badlogic.gdx.physics.box2d.World;

import java.util.HashMap;

/**
 * Created by rene__000 on 3/27/2015.
 */
public class EditorWorldLoader extends EasyWorldLoader {
    public static void LoadEditorWorld(EasyWorldEditor easyWorldEditor){
        LoadWorld(easyWorldEditor.GetXmlFile().path(), easyWorldEditor.world);
        easyWorldEditor.easyBodyMap = easyBodyMap;
    }
}
