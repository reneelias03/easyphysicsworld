package com.easyphysicsworld.editor;

import box2dLight.RayHandler;
import box2dLight.PointLight;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.utils.XmlWriter;
import com.badlogic.gdx.math.Rectangle;
import javafx.scene.shape.Circle;
import org.w3c.dom.css.Rect;

//import java.awt.*;
import javax.management.Query;
import javax.swing.*;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created by rene__000 on 3/27/2015.
 */
public class EasyWorldEditor {
    public World world;
    private GameButtons createPolygonButton, savePolygonButton, showGridButton, exportWorldButton, deleteBodyButton, copyBodyButton, debugRendererButton, rotateBodyButton, rotateLeftButton, rotateRightButton, createCircleButton, resetRotationButton, editBodyButton;
    private boolean editorActive;
    private boolean verticeActive;
    private boolean savingPolygon, copyingPolygon, rotatingPolygon, firstRotationPress, creatingCircle, editingPolygon;
    private boolean controlsActive;
    public boolean ControlsActive() {return controlsActive;}
    private boolean showGrid, movingBody, showDebugRenderer;
    public boolean shouldStep;
    private boolean leftMouseClicked, previousLeftMouseClicked;
    private float initialRotateAngle, differenceRotateAngle, polygonInitialAngle, worldExportedSpriteAlpha;
    private ArrayList<Sprite> tempVerticesList;
    HashMap<String, EasyBody> easyBodyMap;
    private Body hitBody;
    private EasyBody selectedEasyBody;
    private Vector2 testPoint, initialClickMoveLocation, initialBodyLocation;
    private BitmapFont ubuntuFont;
    private PolygonSpriteBatch polygonSpriteBatch;
    private Sprite rotate15Sprite;
    private Sprite worldExportedPromptSprite;
    private OrthographicCamera camera;
    public OrthographicCamera getCamera(){
        return camera;
    }


    public void LoadBodies(HashMap<String, EasyBody> bodyMap) {
        easyBodyMap = bodyMap;
    }

    public HashMap<String, EasyBody> GetBodies() {
        return easyBodyMap;
    }

    private Box2DDebugRenderer debugRenderer;
    private AssetManager assetManager;
    private SpriteBatch batch, staticBatch;
    private Sprite gridSprite;
    private int screenWidth, screenHeight;
    private BitmapFont font;
    private XmlWriter xmlWriter;
    private FileHandle file;
    public FileHandle GetXmlFile(){
        return file;
    }
    private Writer writer;
    private PolygonEditorDialog polygonEditorDialog;
    private DecimalFormat decimalFormat;

    public EasyWorldEditor(String xmlFile){
        this(new Vector2(0, -9.8f), xmlFile);
    }

    public EasyWorldEditor(Vector2 gravity, String xmlFile) {
        world = new World(gravity, false);
        showGrid = false;
        verticeActive = false;
        editorActive = false;
        savingPolygon = false;
        rotatingPolygon = false;
        firstRotationPress = false;
        movingBody = false;
        leftMouseClicked = false;
        previousLeftMouseClicked = false;
        copyingPolygon = false;
        editingPolygon = false;
        showDebugRenderer = true;
        controlsActive = true;
        shouldStep = true;
        batch = new SpriteBatch();
        staticBatch = new SpriteBatch();
        polygonSpriteBatch = new PolygonSpriteBatch();
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        assetManager = new AssetManager();
        LoadAssets();
        debugRenderer = new Box2DDebugRenderer();

        camera = new OrthographicCamera(screenWidth, screenHeight);
        camera.position.set(screenWidth / 2f, screenHeight / 2f, 0);
        camera.zoom = .01f;
        camera.update();

        tempVerticesList = new ArrayList<Sprite>();
        createPolygonButton = new GameButtons(GetTextureAsset("EditorButton.png"), true, new Vector2(screenWidth - 220, screenHeight - 70));
        savePolygonButton = new GameButtons(GetTextureAsset("SavePolygonButton.png"), true, new Vector2(screenWidth - 220, screenHeight - 150));
        showGridButton = new GameButtons(GetTextureAsset("ShowGridButton.png"), true, new Vector2(screenWidth - createPolygonButton.getWidth() * 2.25f, screenHeight - 70));
        exportWorldButton = new GameButtons(GetTextureAsset("ExportWorldButton.png"), true, new Vector2(showGridButton.getX(), savePolygonButton.getY()));
        deleteBodyButton = new GameButtons(GetTextureAsset("DeleteBodyButton.png"), true, new Vector2(20, screenHeight - 70));
        copyBodyButton = new GameButtons(GetTextureAsset("CopyBodyButton.png"), true, new Vector2(20, screenHeight - deleteBodyButton.getHeight() * 1.5f - 70));
        rotateBodyButton = new GameButtons(GetTextureAsset("RotateBodyButton.png"), true, new Vector2(20, copyBodyButton.getY() - copyBodyButton.getHeight() * 1.5f));
        rotate15Sprite = new Sprite(GetTextureAsset("Rotate15Button.png"));
        rotate15Sprite.setPosition(rotateBodyButton.getX(), rotateBodyButton.getY() - rotateBodyButton.getHeight() * 1.5f);
        rotateLeftButton = new GameButtons(GetTextureAsset("RotateLeftButton.png"), true, new Vector2(rotateBodyButton.getX(), rotate15Sprite.getY() - rotate15Sprite.getHeight()));
        rotateLeftButton.setSize(rotateLeftButton.getWidth(), rotateLeftButton.getHeight() * .75f);
        rotateRightButton = new GameButtons(GetTextureAsset("RotateRightButton.png"), true, new Vector2(rotateBodyButton.getX() + rotateLeftButton.getWidth(), rotate15Sprite.getY() - rotate15Sprite.getHeight()));
        rotateRightButton.setSize(rotateRightButton.getWidth(), rotateRightButton.getHeight() * .75f);
        debugRendererButton = new GameButtons(GetTextureAsset("DebugRendererButton.png"), true, new Vector2(screenWidth - 220, 20f));
        createCircleButton = new GameButtons(GetTextureAsset("CreateCircleButton.png"), true, new Vector2(savePolygonButton.getX(), savePolygonButton.getY() - savePolygonButton.getHeight() * 1.5f));
        resetRotationButton = new GameButtons(GetTextureAsset("ResetRotationButton.png"), true, new Vector2(rotateBodyButton.getX(), rotateLeftButton.getY() - rotateBodyButton.getHeight() * 1.5f));
        editBodyButton = new GameButtons(GetTextureAsset("EditBodyButton.png"), true, new Vector2(deleteBodyButton.getX() + deleteBodyButton.getWidth() + 20, deleteBodyButton.getY()));
        gridSprite = new Sprite(GetTextureAsset("WhitePixel.png"));
        worldExportedPromptSprite = new Sprite(GetTextureAsset("WorldExportedPrompt.png"));
        worldExportedPromptSprite.setOriginCenter();
        worldExportedPromptSprite.setPosition(screenWidth / 2 - worldExportedPromptSprite.getOriginX(), screenHeight / 2 - worldExportedPromptSprite.getOriginY());

        polygonEditorDialog = new PolygonEditorDialog(GetTextureAsset("Window.png"), assetManager);

        file = new FileHandle(xmlFile);

        font = new BitmapFont();
        ubuntuFont = new BitmapFont(Gdx.files.local("UbuntuMedium.fnt"), new TextureRegion(GetTextureAsset("UbuntuMedium.png")), false);

        initialRotateAngle = 0;
        differenceRotateAngle = 0;

        decimalFormat = new DecimalFormat("#.00");
        worldExportedSpriteAlpha = 0;
        worldExportedPromptSprite.setAlpha(worldExportedSpriteAlpha);
    }

    public EasyBody GetBody(String bodyName) {
        return easyBodyMap.get(bodyName);
    }

    public void Update() {
        leftMouseClicked = Gdx.input.isButtonPressed(Input.Buttons.LEFT);
        createPolygonButton.Update();

        if (Gdx.input.isKeyPressed(Input.Keys.Z) && !savingPolygon && !copyingPolygon && !editingPolygon) {
            if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))) {
                camera.zoom = .01f;
            } else {
                camera.zoom += .0002f;
                for (int i = 0; i < tempVerticesList.size(); i++) {
                    if (tempVerticesList.get(i).getWidth() != 10f * camera.zoom) {
                        tempVerticesList.get(i).setSize(10f * camera.zoom, 10f * camera.zoom);
                    }
                }
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.X) && !savingPolygon && !copyingPolygon && !editingPolygon) {
            camera.zoom -= .0002f;
            for (int i = 0; i < tempVerticesList.size(); i++) {
                if (tempVerticesList.get(i).getWidth() != 10f * camera.zoom) {
                    tempVerticesList.get(i).setSize(10f * camera.zoom, 10f * camera.zoom);
                }
            }
        }

        // Up Controls
        if(Gdx.input.isKeyPressed(Input.Keys.UP)){
            if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && selectedEasyBody != null && !showGrid){
                selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x, selectedEasyBody.body.getPosition().y + 1f * camera.zoom, selectedEasyBody.body.getAngle());
            } else if(!(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))){
                camera.position.y += 20f * camera.zoom;
            }
        }
        if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && Gdx.input.isKeyJustPressed(Input.Keys.UP) && selectedEasyBody != null && showGrid) {
            selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x, selectedEasyBody.body.getPosition().y + 20f * camera.zoom, selectedEasyBody.body.getAngle());
            MoveByGrid(selectedEasyBody);
        }

        //Down Controls
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && selectedEasyBody != null && !showGrid){
                    selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x, selectedEasyBody.body.getPosition().y - 1f * camera.zoom, selectedEasyBody.body.getAngle());
            } else if(!(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))){
                camera.position.y -= 20f * camera.zoom;
            }
        }
        if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && Gdx.input.isKeyJustPressed(Input.Keys.DOWN) && selectedEasyBody != null && showGrid) {
            selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x, selectedEasyBody.body.getPosition().y - 20f * camera.zoom, selectedEasyBody.body.getAngle());
            MoveByGrid(selectedEasyBody);
        }

        //Right Controls
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && selectedEasyBody != null && !showGrid){
                selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x + 1f * camera.zoom, selectedEasyBody.body.getPosition().y, selectedEasyBody.body.getAngle());
            } else if(!(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))){
                camera.position.x += 20f * camera.zoom;
            }
        }
        if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && Gdx.input.isKeyJustPressed(Input.Keys.RIGHT) && selectedEasyBody != null && showGrid) {
            selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x + 20f * camera.zoom, selectedEasyBody.body.getPosition().y, selectedEasyBody.body.getAngle());
            MoveByGrid(selectedEasyBody);
        }

        //Left Controls
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && selectedEasyBody != null && !showGrid){
                selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x - 1f * camera.zoom, selectedEasyBody.body.getPosition().y, selectedEasyBody.body.getAngle());
            } else if(!(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT))){
                camera.position.x -= 20f * camera.zoom;
            }
        }
        if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && Gdx.input.isKeyJustPressed(Input.Keys.LEFT) && selectedEasyBody != null && showGrid) {
            selectedEasyBody.body.setTransform(selectedEasyBody.body.getPosition().x - 20f * camera.zoom, selectedEasyBody.body.getPosition().y, selectedEasyBody.body.getAngle());
            MoveByGrid(selectedEasyBody);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.G)) {
            showGrid = !showGrid;
        }

        if((Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) && Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            showDebugRenderer = !showDebugRenderer;
        }

        if ((createPolygonButton.Tapped() || Gdx.input.isKeyJustPressed(Input.Keys.E)) && !savingPolygon && !copyingPolygon && !editingPolygon && !Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
            editorActive = !editorActive;
        }
        if (selectedEasyBody != null) {
            deleteBodyButton.Update();
            if (deleteBodyButton.Tapped() || Gdx.input.isKeyJustPressed(Input.Keys.FORWARD_DEL)) {
                easyBodyMap.remove(selectedEasyBody.GetName(), selectedEasyBody);
                world.destroyBody(selectedEasyBody.body);
                selectedEasyBody = null;
            }
            copyBodyButton.Update();
            if (!copyingPolygon && (copyBodyButton.Tapped() || (Gdx.input.isKeyPressed(Input.Keys.C) && Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)))) {
                if (selectedEasyBody.body.getFixtureList().get(0).getShape().getType() == Shape.Type.Polygon) {
                    for (int i = 0; i < selectedEasyBody.getVertices().length; i++) {
                        Sprite newVertice = new Sprite(GetTextureAsset("EmptyCircle.png"));
                        newVertice.setSize(10 * camera.zoom, 10 * camera.zoom);
                        newVertice.setOriginCenter();
                        newVertice.setPosition(selectedEasyBody.getVertices()[i].x + selectedEasyBody.body.getPosition().x, selectedEasyBody.getVertices()[i].y + selectedEasyBody.body.getPosition().y);
                        tempVerticesList.add(newVertice);
                    }
                } else {
                    Sprite newVertice = new Sprite(GetTextureAsset("EmptyCircle.png"));
                    newVertice.setSize(10 * camera.zoom, 10 * camera.zoom);
                    newVertice.setOriginCenter();
                    newVertice.setPosition(selectedEasyBody.body.getPosition().x - selectedEasyBody.body.getFixtureList().get(0).getShape().getRadius(), selectedEasyBody.body.getPosition().y + selectedEasyBody.body.getFixtureList().get(0).getShape().getRadius());
                    tempVerticesList.add(newVertice);

                    Sprite newVertice2 = new Sprite(GetTextureAsset("EmptyCircle.png"));
                    newVertice2.setSize(10 * camera.zoom, 10 * camera.zoom);
                    newVertice2.setOriginCenter();
                    newVertice2.setPosition(selectedEasyBody.body.getPosition().x + selectedEasyBody.body.getFixtureList().get(0).getShape().getRadius(), selectedEasyBody.body.getPosition().y + selectedEasyBody.body.getFixtureList().get(0).getShape().getRadius());
                    tempVerticesList.add(newVertice2);
                    creatingCircle = true;
                }
                copyingPolygon = true;
                polygonEditorDialog.CopyValues(selectedEasyBody, false);
                controlsActive = false;
            }
            editBodyButton.Update();
            if((editBodyButton.Tapped() || (Gdx.input.isKeyPressed(Input.Keys.E) && Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT))) && !editingPolygon){
                editingPolygon = true;
                polygonEditorDialog.CopyValues(selectedEasyBody, true);
                controlsActive = false;
            }
            if(editingPolygon)
            {
                polygonEditorDialog.Update();
                if(polygonEditorDialog.GetExit()){
                    editingPolygon = false;
                    polygonEditorDialog.ResetExit();
                    controlsActive = true;
                }
                if(polygonEditorDialog.Created()) {
                    EditBody(selectedEasyBody);
                    controlsActive = true;
                }
            }
            rotateBodyButton.Update();
            if (rotateBodyButton.Tapped()) {
                rotatingPolygon = !rotatingPolygon;
                if (!rotatingPolygon) {
                    firstRotationPress = false;
                }
                else
                {
                    Rectangle roughRectangle = CalculateRoughBodyRectangle(selectedEasyBody);
                    initialX = selectedEasyBody.body.getPosition().x + roughRectangle.getWidth();
                    initialY = selectedEasyBody.body.getPosition().y + roughRectangle.getHeight();
                    initialCenterX = initialX + roughRectangle.getWidth() / 2f;
                    initialCenterY = initialY + roughRectangle.getHeight() / 2f;
                }
            }
            if (rotatingPolygon) {
                rotateLeftButton.Update();
                rotateRightButton.Update();
                resetRotationButton.Update();
                if (rotateLeftButton.Tapped()) {
                    RotateBody(selectedEasyBody, true, false);
                }
                else if (rotateRightButton.Tapped()) {
                    RotateBody(selectedEasyBody, false, true);
                }
                else if (resetRotationButton.Tapped()) {
                    Rectangle roughRectangle = CalculateRoughBodyRectangle(selectedEasyBody);
                    selectedEasyBody.body.setTransform(initialX - roughRectangle.getWidth(), initialY - roughRectangle.getHeight(), 0);
                }
                else if(!GestureManager.IsCurrentlyBeingTouched(deleteBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(copyBodyButton.getBoundingRectangle())
                        && !GestureManager.IsCurrentlyBeingTouched(rotateBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(debugRendererButton.getBoundingRectangle()) &&
                        !GestureManager.IsCurrentlyBeingTouched(editBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(showGridButton.getBoundingRectangle()) &&
                        !GestureManager.IsCurrentlyBeingTouched(rotateLeftButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(rotateRightButton.getBoundingRectangle())){
                    RotateBody(selectedEasyBody, false, false);
                }
            }
        }
        if (copyingPolygon) {
            polygonEditorDialog.Update();
            if(!controlsActive) {
                controlsActive = false;
            }
            if (polygonEditorDialog.GetExit()) {
                tempVerticesList.clear();
                copyingPolygon = false;
                polygonEditorDialog.ResetExit();
                controlsActive = true;
            }
            if (polygonEditorDialog.Created()) {
                SavePolygon();
                controlsActive = true;
            }
        }

        showGridButton.Update();
        if (showGridButton.Tapped()) {
            showGrid = !showGrid;
        }

        debugRendererButton.Update();
        if (debugRendererButton.Tapped()) {
            showDebugRenderer = !showDebugRenderer;
        }

        if (editorActive) {
            savePolygonButton.Update();
            if (savePolygonButton.Tapped()) {
                if (tempVerticesList.size() > 2) {
                    savingPolygon = true;
                    controlsActive = false;
                }
            }
            exportWorldButton.Update();
            if (exportWorldButton.Tapped()) {
                try {
                    ExportWorld();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (tempVerticesList.size() == 2) {
                createCircleButton.Update();
                if (createCircleButton.Tapped()) {
                    savingPolygon = true;
                    creatingCircle = true;
                }
            }
            if (savingPolygon) {
                polygonEditorDialog.Update();
                if (polygonEditorDialog.GetExit()) {
                    tempVerticesList.clear();
                    savingPolygon = false;
                    polygonEditorDialog.ResetExit();
                    controlsActive = true;
                }
                if (polygonEditorDialog.Created()) {
                    SavePolygon();
                    controlsActive = true;
                }
            } else {
                PolygonEditor();
            }
        } else {
            if (movingBody) {
                selectedEasyBody.body.setAwake(false);
                Rectangle tempBodyRectangle = CalculateRoughBodyRectangle(selectedEasyBody);
                Vector2 dragLocation = GestureManager.DragLocation(tempBodyRectangle.width, tempBodyRectangle.height, camera.zoom, new Vector2(camera.position.x, camera.position.y));
                selectedEasyBody.body.setTransform(initialBodyLocation.x - (initialClickMoveLocation.x - dragLocation.x), initialBodyLocation.y - (initialClickMoveLocation.y - dragLocation.y), selectedEasyBody.body.getAngle());
            }
            if (movingBody & !Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                movingBody = false;
                initialClickMoveLocation = null;
                if (showGrid) {
                    MoveByGrid(selectedEasyBody);
                }

            } else if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                CheckBodyTouch();
            }
            if(shouldStep) {
                world.step(1 / 60f, 6, 2);
            }
        }
        if(worldExportedSpriteAlpha > 0.01f){
            worldExportedSpriteAlpha -= .01f;
            worldExportedPromptSprite.setAlpha(worldExportedSpriteAlpha);
        }

        previousLeftMouseClicked = leftMouseClicked;
    }

    public void Draw() {

        //World Draws
        batch.setProjectionMatrix(camera.combined);
        if (showDebugRenderer) {
            debugRenderer.render(world, camera.combined);
        }
        batch.begin();


        for (Sprite vertice : tempVerticesList) {
            vertice.draw(batch);
        }
        batch.end();

        //Static draws
        staticBatch.begin();
        if (showGrid) {
            gridSprite.setSize(2f, screenHeight);
            gridSprite.setAlpha(.25f);
            Vector2 fontPosition = new Vector2();
            for (float i = 0; i < screenWidth; i += 20f) {
                gridSprite.setPosition(i, 0);
                gridSprite.draw(staticBatch);
                fontPosition.x = gridSprite.getX() * camera.zoom + camera.position.x - Gdx.graphics.getWidth() * camera.zoom / 2;
                if(i % 50 == 0) {
                    font.draw(staticBatch, String.format(decimalFormat.format(fontPosition.x)), gridSprite.getX(), 20);
                }
            }
            gridSprite.setSize(screenWidth, 2f);
            for (float i = 0; i < screenHeight; i += 20f) {
                gridSprite.setPosition(0, i);
                gridSprite.draw(staticBatch);
                fontPosition.y = gridSprite.getY() * camera.zoom + camera.position.y - Gdx.graphics.getHeight() * camera.zoom / 2;
                if(i % 50 == 0) {
                    font.draw(staticBatch, String.format(decimalFormat.format(fontPosition.y)), gridSprite.getX(), gridSprite.getY());
                }
            }
        }
        if (editorActive) {
            font.draw(staticBatch, "Editor Active: True", createPolygonButton.getX(), createPolygonButton.getY() - 5);
            savePolygonButton.draw(staticBatch);
            exportWorldButton.draw(staticBatch);
            if (tempVerticesList.size() == 2) {
                createCircleButton.draw(staticBatch);
            }
        } else {
            font.draw(staticBatch, "Editor Active: False", createPolygonButton.getX(), createPolygonButton.getY() - 5);
        }

        if (selectedEasyBody != null) {
            ubuntuFont.setScale(.75f);
            Vector2 staticBodyPosition = new Vector2((selectedEasyBody.body.getPosition().x + (Gdx.graphics.getWidth() * camera.zoom) / 2 - camera.position.x) / camera.zoom, (selectedEasyBody.body.getPosition().y + (Gdx.graphics.getHeight() * camera.zoom) / 2 - camera.position.y) / camera.zoom);
            Rectangle roughRectangle = CalculateRoughBodyRectangle(selectedEasyBody);
            ubuntuFont.draw(staticBatch, String.format("w: %f, h: %f", (float) (roughRectangle.getWidth() / camera.zoom), (float) (roughRectangle.getHeight() / camera.zoom)), staticBodyPosition.x, staticBodyPosition.y + roughRectangle.getHeight() / camera.zoom + ubuntuFont.getLineHeight());
            ubuntuFont.draw(staticBatch, selectedEasyBody.GetName(), staticBodyPosition.x, staticBodyPosition.y);
            ubuntuFont.draw(staticBatch, String.format("x: " + decimalFormat.format(selectedEasyBody.body.getPosition().x) + ", y: " + decimalFormat.format(selectedEasyBody.body.getPosition().y)), staticBodyPosition.x, staticBodyPosition.y - 40);
            deleteBodyButton.draw(staticBatch);
            editBodyButton.draw(staticBatch);
            copyBodyButton.draw(staticBatch);
            rotateBodyButton.draw(staticBatch);
            font.draw(staticBatch, String.format("Rotating Body: " + rotatingPolygon), rotateBodyButton.getX(), rotateBodyButton.getY());
            if (rotatingPolygon) {
                rotate15Sprite.draw(staticBatch);
                rotateLeftButton.draw(staticBatch);
                rotateRightButton.draw(staticBatch);
                resetRotationButton.draw(staticBatch);
//                ubuntuFont.draw(staticBatch, String.format("rw: " + decimalFormat.format(roughRectangle.getWidth()) + ", rh: " + decimalFormat.format(roughRectangle.getHeight())), staticBodyPosition.x, staticBodyPosition.y - 60);
            }
        }
        if (savingPolygon || copyingPolygon || editingPolygon) {
            polygonEditorDialog.Draw(staticBatch);
        }

        showGridButton.draw(staticBatch);
        createPolygonButton.draw(staticBatch);
        debugRendererButton.draw(staticBatch);
        worldExportedPromptSprite.draw(staticBatch);
        staticBatch.end();


    }

    private void PolygonEditor() {
        if (verticeActive) {
            Vector2 verticePosition = GestureManager.DragLocation(tempVerticesList.get(tempVerticesList.size() - 1).getWidth(), tempVerticesList.get(tempVerticesList.size() - 1).getHeight(), camera.zoom, new Vector2(camera.position.x, camera.position.y));

            if (!Gdx.input.isTouched()) {
                verticeActive = false;
                if (showGrid) {
                    Vector2 inputPosition = GestureManager.InputLocation();
                    float step = 20f;
                    float remainder = inputPosition.x  % step;
                    float addAmount = remainder >= step / 2f ? step : 0;
                    float closestX = inputPosition.x - remainder + addAmount;
//                        closestX = (closestX * camera.zoom) + camera.position.x - (camera.zoom * Gdx.graphics.getWidth() / 2);
                    remainder = inputPosition.y % step;
                    addAmount = remainder >= step / 4f ? step : 0;
                    float closestY = inputPosition.y - remainder + addAmount;
//                        closestY = (closestY * camera.zoom) + camera.position.y - (camera.zoom * Gdx.graphics.getHeight() / 2);

                    closestX = closestX * camera.zoom + camera.position.x - Gdx.graphics.getWidth() * camera.zoom / 2;
                    closestY = closestY * camera.zoom + camera.position.y - Gdx.graphics.getHeight() * camera.zoom / 2;

                    verticePosition = new Vector2(closestX - tempVerticesList.get(tempVerticesList.size() - 1).getWidth() / 2, closestY - tempVerticesList.get(0).getOriginY());
                }
            }
            tempVerticesList.get(tempVerticesList.size() - 1).setPosition(verticePosition.x, verticePosition.y);
        } else if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            if (!GestureManager.IsCurrentlyBeingTouched(savePolygonButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(createPolygonButton.getBoundingRectangle())
                    && !GestureManager.IsCurrentlyBeingTouched(showGridButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(exportWorldButton.getBoundingRectangle())
                    && !GestureManager.IsCurrentlyBeingTouched(deleteBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(debugRendererButton.getBoundingRectangle())
                    && !GestureManager.IsCurrentlyBeingTouched(rotateBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(createCircleButton.getBoundingRectangle())) {
                Sprite newVertice = new Sprite(GetTextureAsset("EmptyCircle.png"));
                newVertice.setSize(10 * camera.zoom, 10 * camera.zoom);
                newVertice.setOriginCenter();
                verticeActive = true;
                Vector2 verticePosition = GestureManager.DragLocation(newVertice.getWidth(), newVertice.getHeight(), camera.zoom, new Vector2(camera.position.x, camera.position.y));
                newVertice.setPosition(verticePosition.x, verticePosition.y);
                tempVerticesList.add(newVertice);
            }
        } else if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            for (int i = 0; i < tempVerticesList.size(); i++) {
                if (GestureManager.IsCurrentlyBeingTouched(tempVerticesList.get(i).getBoundingRectangle(), camera.zoom, new Vector2(camera.position.x, camera.position.y))) {
                    tempVerticesList.remove(i);
                }
            }
        }
    }

    private void LoadAssets() {
        assetManager.load("EditorButton.png", Texture.class);
        assetManager.load("SavePolygonButton.png", Texture.class);
        assetManager.load("EmptyCircle.png", Texture.class);
        assetManager.load("WhitePixel.png", Texture.class);
        assetManager.load("ShowGridButton.png", Texture.class);
        assetManager.load("ExportWorldButton.png", Texture.class);
        assetManager.load("DeleteBodyButton.png", Texture.class);
        assetManager.load("CopyBodyButton.png", Texture.class);
        assetManager.load("DebugRendererButton.png", Texture.class);
        assetManager.load("RotateBodyButton.png", Texture.class);
        assetManager.load("Rotate15Button.png", Texture.class);
        assetManager.load("RotateLeftButton.png", Texture.class);
        assetManager.load("RotateRightButton.png", Texture.class);
        assetManager.load("CreateCircleButton.png", Texture.class);
        assetManager.load("ResetRotationButton.png", Texture.class);
        assetManager.load("EditBodyButton.png", Texture.class);
        assetManager.load("IconBody.png", Texture.class);
        assetManager.load("IconDensity.png", Texture.class);
        assetManager.load("IconFriction.png", Texture.class);
        assetManager.load("IconRestitution.png", Texture.class);
        assetManager.load("LineBreak.png", Texture.class);
        assetManager.load("NameLine.png", Texture.class);
        assetManager.load("SpriteSheet.png", Texture.class);
        assetManager.load("TextBox.png", Texture.class);
        assetManager.load("Window.png", Texture.class);
        assetManager.load("UbuntuMedium.png", Texture.class);
        assetManager.load("UbuntuLight.png", Texture.class);
        assetManager.load("UbuntuMediumTiny.png", Texture.class);
        assetManager.load("WorldExportedPrompt.png", Texture.class);
        assetManager.finishLoading();
    }

    public Texture GetTextureAsset(String fileName) {
        return assetManager.get(fileName, Texture.class);
    }

    private void SavePolygon() {
        String[] bodyInfo = polygonEditorDialog.GetPolygonInfo();
        BodyDef newBodyDef = new BodyDef();

        newBodyDef.type  = BodyDef.BodyType.valueOf(bodyInfo[1] + "Body");
        newBodyDef.allowSleep = false;
        newBodyDef.awake = Boolean.parseBoolean(bodyInfo[5]);
        newBodyDef.active = Boolean.parseBoolean(bodyInfo[6]);

        FixtureDef newFixtureDef = new FixtureDef();
        if (!creatingCircle) {
            float minVerticeX = 0, minVerticeY = 0;
            Vector2[] verticesArray = new Vector2[tempVerticesList.size()];
            for (int i = 0; i < verticesArray.length; i++) {
                verticesArray[i] = new Vector2(tempVerticesList.get(i).getX() + tempVerticesList.get(i).getOriginX(), tempVerticesList.get(i).getY() + tempVerticesList.get(i).getOriginY());
                if (i == 0) {
                    minVerticeX = verticesArray[i].x;
                    minVerticeY = verticesArray[i].y;
                } else {
                    if (verticesArray[i].x < minVerticeX) {
                        minVerticeX = verticesArray[i].x;
                    }
                    if (verticesArray[i].y < minVerticeY) {
                        minVerticeY = verticesArray[i].y;
                    }
                }
            }
            for (int i = 0; i < verticesArray.length; i++) {
                verticesArray[i].x -= minVerticeX;
                verticesArray[i].y -= minVerticeY;
            }

            newBodyDef.position.x = minVerticeX;
            newBodyDef.position.y = minVerticeY;

            PolygonShape newPolygonShape = new PolygonShape();
            newPolygonShape.set(verticesArray);

            newFixtureDef.shape = newPolygonShape;
            newFixtureDef.friction = Float.parseFloat(bodyInfo[2]);
            newFixtureDef.density = Float.parseFloat(bodyInfo[3]);
            newFixtureDef.restitution = Float.parseFloat(bodyInfo[4]);

            easyBodyMap.put(bodyInfo[0], new EasyBody(world, newBodyDef, newFixtureDef, bodyInfo[0]));
            easyBodyMap.get(bodyInfo[0]).setVertices(verticesArray);
            newPolygonShape.dispose();
        } else {
            float radius = (Math.abs(tempVerticesList.get(0).getX() - tempVerticesList.get(1).getX())) / 2;
            float x, y;
            if (tempVerticesList.get(0).getX() < tempVerticesList.get(1).getX()) {
                x = tempVerticesList.get(0).getX() + radius;
            } else {
                x = tempVerticesList.get(1).getX() + radius;
            }

            if (tempVerticesList.get(0).getY() < tempVerticesList.get(1).getY()) {
                y = tempVerticesList.get(0).getY();
            } else {
                y = tempVerticesList.get(1).getY();
            }
            newBodyDef.position.x = x;
            newBodyDef.position.y = y;

            CircleShape circleShape = new CircleShape();
            circleShape.setRadius(radius);

            newFixtureDef.shape = circleShape;
            newFixtureDef.density = Float.parseFloat(bodyInfo[3]);
            newFixtureDef.restitution = Float.parseFloat(bodyInfo[4]);

            easyBodyMap.put(bodyInfo[0], new EasyBody(world, newBodyDef, newFixtureDef, bodyInfo[0]));
            easyBodyMap.get(bodyInfo[0]).body.setAngularDamping(Float.parseFloat(bodyInfo[2]));
            circleShape.dispose();
        }


        tempVerticesList.clear();
        savingPolygon = false;
        if(copyingPolygon) {
            selectedEasyBody = easyBodyMap.get(bodyInfo[0]);
            copyingPolygon = false;
        }
        creatingCircle = false;
        polygonEditorDialog.ResetValues();
    }

    private void CheckBodyTouch() {
        if (!savingPolygon && !copyingPolygon && !rotatingPolygon && !editingPolygon) {
            testPoint = GestureManager.InputLocation(0, camera.zoom, new Vector2(camera.position.x, camera.position.y));
            hitBody = null;
            world.QueryAABB(callback, testPoint.x - .01f, testPoint.y - .01f, testPoint.x + .01f, testPoint.y + .01f);

            if (hitBody != null) {
                if ((selectedEasyBody == null || selectedEasyBody.body != hitBody) && !GestureManager.IsCurrentlyBeingTouched(deleteBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(copyBodyButton.getBoundingRectangle())
                        && !GestureManager.IsCurrentlyBeingTouched(rotateBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(debugRendererButton.getBoundingRectangle()) &&
                        !GestureManager.IsCurrentlyBeingTouched(editBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(showGridButton.getBoundingRectangle())) {
                    if (selectedEasyBody != null) {
                        selectedEasyBody.body.setAwake(true);
                    }
                    for (EasyBody easyBody : easyBodyMap.values()) {
                        if (easyBody.body == hitBody) {
                            selectedEasyBody = easyBody;
                            break;
                        }
                    }
                } else {
                    if (!movingBody && selectedEasyBody != null) {
                        movingBody = true;
                        Rectangle roughRectangle = CalculateRoughBodyRectangle(selectedEasyBody);
                        initialClickMoveLocation = GestureManager.DragLocation(roughRectangle.width, roughRectangle.height, camera.zoom, new Vector2(camera.position.x, camera.position.y));
                        initialBodyLocation = new Vector2(selectedEasyBody.body.getPosition().x, selectedEasyBody.body.getPosition().y);
                    }
                }
            } else {
                if (selectedEasyBody != null && !GestureManager.IsCurrentlyBeingTouched(deleteBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(copyBodyButton.getBoundingRectangle())
                        && !GestureManager.IsCurrentlyBeingTouched(rotateBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(debugRendererButton.getBoundingRectangle()) &&
                        !GestureManager.IsCurrentlyBeingTouched(editBodyButton.getBoundingRectangle()) && !GestureManager.IsCurrentlyBeingTouched(showGridButton.getBoundingRectangle()) && !editingPolygon && !movingBody && !rotatingPolygon) {
                    selectedEasyBody.body.setAwake(true);
                    selectedEasyBody = null;
                }
            }
        }
    }

    QueryCallback callback = new QueryCallback() {
        @Override
        public boolean reportFixture(Fixture fixture) {
//            if (fixture.getBody() == easyBodyMap.get("ground").body || fixture.getBody() == easyBodyMap.get("greenCircle").body) {
//                return true;
//            }
            if (fixture.testPoint(testPoint.x, testPoint.y)) {
                hitBody = fixture.getBody();
                return false;
            } else {
                return true;
            }
        }
    };

    private float initialX, initialY, initialCenterX, initialCenterY;
    private void RotateBody(EasyBody easyBody, boolean leftButton, boolean rightButton) {
        Rectangle roughRectangle = CalculateRoughBodyRectangle(easyBody);
        Vector2 radiusAngle = new Vector2(roughRectangle.getWidth() / 2f, roughRectangle.getHeight() / 2f);
        Vector2 centerDiffVector;

        if(leftButton) {
            polygonInitialAngle = easyBody.body.getAngle();
            radiusAngle.setAngle(polygonInitialAngle * MathUtils.radiansToDegrees + radiusAngle.angle() + 15f);
            centerDiffVector = new Vector2(initialCenterX - initialX + radiusAngle.x, initialCenterY - initialY + radiusAngle.y);
            easyBody.body.setTransform(initialX - centerDiffVector.x, initialY - centerDiffVector.y, polygonInitialAngle + 15f * MathUtils.degreesToRadians);
        } else if(rightButton) {
            polygonInitialAngle = easyBody.body.getAngle();
            radiusAngle.setAngle(polygonInitialAngle * MathUtils.radiansToDegrees + radiusAngle.angle() - 15f);
            centerDiffVector = new Vector2(initialCenterX - initialX + radiusAngle.x, initialCenterY - initialY + radiusAngle.y);
            easyBody.body.setTransform(initialX - centerDiffVector.x, initialY - centerDiffVector.y, polygonInitialAngle - 15f * MathUtils.degreesToRadians);
        } else {
            if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                if (!firstRotationPress) {
                    polygonInitialAngle = easyBody.body.getAngle();
                    firstRotationPress = true;
                } else {
                    differenceRotateAngle = GestureManager.InputLocation(0, camera.zoom, new Vector2(camera.position.x, camera.position.y)).sub(new Vector2(roughRectangle.x + roughRectangle.getWidth() / 2, roughRectangle.y + roughRectangle.getHeight() / 2)).angle() - initialRotateAngle;
                    radiusAngle.setAngle(polygonInitialAngle * MathUtils.radiansToDegrees + radiusAngle.angle() + differenceRotateAngle);
                    centerDiffVector = new Vector2(initialCenterX - initialX + radiusAngle.x, initialCenterY - initialY + radiusAngle.y);
                    easyBody.body.setTransform(initialX - centerDiffVector.x, initialY - centerDiffVector.y, polygonInitialAngle + differenceRotateAngle * MathUtils.degreesToRadians);
                }
            } else {
                if (firstRotationPress) {
                    firstRotationPress = false;
                }
            }
        }
    }

    public Rectangle CalculateRoughBodyRectangle(EasyBody easyBody) {
        if (easyBody.body.getFixtureList().get(0).getShape().getType() == Shape.Type.Polygon) {
            float minX = 0, maxX = 0, minY = 0, maxY = 0;
            for (int i = 0; i < easyBody.getVertices().length; i++) {
                if (i == 0) {
                    minX = easyBody.getVertices()[i].x;
                    maxX = easyBody.getVertices()[i].x;
                    minY = easyBody.getVertices()[i].y;
                    maxY = easyBody.getVertices()[i].y;
                }
                if (easyBody.getVertices()[i].x > maxX) {
                    maxX = easyBody.getVertices()[i].x;
                }
                if (easyBody.getVertices()[i].x < minX) {
                    minX = easyBody.getVertices()[i].x;
                }
                if (easyBody.getVertices()[i].y > maxY) {
                    maxY = easyBody.getVertices()[i].y;
                }
                if (easyBody.getVertices()[i].y < minY) {
                    minY = easyBody.getVertices()[i].y;
                }
            }
            return new Rectangle(easyBody.body.getPosition().x, easyBody.body.getPosition().y, maxX - minX, maxY - minY);
        } else {
            float x = easyBody.body.getPosition().x - easyBody.body.getFixtureList().get(0).getShape().getRadius(), y = easyBody.body.getPosition().y - easyBody.body.getFixtureList().get(0).getShape().getRadius();
            return new Rectangle(x, y, easyBody.body.getFixtureList().get(0).getShape().getRadius() * 2, easyBody.body.getFixtureList().get(0).getShape().getRadius() * 2);
        }
    }

    private void EditBody(EasyBody easyBody)
    {
        String[] bodyInfo = polygonEditorDialog.GetPolygonInfo();

        easyBody.body.setType(BodyDef.BodyType.valueOf(bodyInfo[1] + "Body"));
        if(easyBody.body.getFixtureList().get(0).getShape().getType() == Shape.Type.Polygon) {
            easyBody.body.getFixtureList().get(0).setFriction(Float.parseFloat(bodyInfo[2]));
        } else {
            easyBody.body.setAngularDamping(Float.parseFloat(bodyInfo[2]));
        }
        easyBody.body.getFixtureList().get(0).setDensity(Float.parseFloat(bodyInfo[3]));
        easyBody.body.getFixtureList().get(0).setRestitution(Float.parseFloat(bodyInfo[4]));
        easyBody.body.setAwake(Boolean.parseBoolean(bodyInfo[5]));
        easyBody.body.setActive(Boolean.parseBoolean(bodyInfo[6]));

        editingPolygon = false;
        polygonEditorDialog.ResetValues();
    }

    private void MoveByGrid(EasyBody easyBody)
    {
        Vector2 snapPosition = new Vector2((easyBody.body.getPosition().x - camera.position.x + Gdx.graphics.getWidth() * camera.zoom / 2f) / camera.zoom,
                (easyBody.body.getPosition().y - camera.position.y + Gdx.graphics.getHeight() * camera.zoom / 2f) / camera.zoom);
        float step = 20f;
        float remainder = snapPosition.x % step;
        float addAmount = remainder >= step / 2f ? step : 0;
        float closestX = snapPosition.x - remainder + addAmount;
        remainder = snapPosition.y % step;
        addAmount = remainder >= step / 2f ? step : 0;
        float closestY = snapPosition.y - remainder + addAmount;
        closestX = closestX * camera.zoom + camera.position.x - Gdx.graphics.getWidth() * camera.zoom / 2;
        closestY = closestY * camera.zoom + camera.position.y - Gdx.graphics.getHeight() * camera.zoom / 2;
        easyBody.body.setTransform(closestX, closestY, selectedEasyBody.body.getAngle());
    }

    private void ExportWorld() throws IOException {
        writer = file.writer(false);
        xmlWriter = new XmlWriter(writer);
        xmlWriter.element("Bodies");
        for (EasyBody easyBody : easyBodyMap.values()) {
            if (easyBody.body.getFixtureList().get(0).getShape().getType() == Shape.Type.Circle) {
                xmlWriter.element("Body")
                        .element("Name")
                        .text(easyBody.GetName())
                        .pop()
                        .element("X")
                        .text(easyBody.body.getPosition().x)
//                        .text(0)
                        .pop()
                        .element("Y")
                        .text(easyBody.body.getPosition().y)
//                        .text(0)
                        .pop()
                        .element("Shape")
                        .text(easyBody.body.getFixtureList().get(0).getShape().getType())
                        .pop()
                        .element("Radius")
                        .text(easyBody.body.getFixtureList().get(0).getShape().getRadius())
                        .pop()
                        .element("BodyType")
                        .text(easyBody.body.getType())
                        .pop()
                        .element("AngularDamping")
                        .text(easyBody.body.getAngularDamping())
                        .pop()
                        .element("Density")
                        .text(easyBody.body.getFixtureList().get(0).getDensity())
                        .pop()
                        .element("Restitution")
                        .text(easyBody.body.getFixtureList().get(0).getRestitution())
                        .pop()
                        .element("Awake")
                        .text(easyBody.body.isAwake())
                        .pop()
                        .element("Active")
                        .text(easyBody.body.isActive())
                        .pop()
                        .element("Angle")
                        .text(easyBody.body.getAngle())
                        .pop()
                        .pop();
                xmlWriter.flush();
            } else {
                XmlWriter xmlVertices = xmlWriter.element("Body")
                        .element("Name")
                        .text(easyBody.GetName())
                        .pop()
                        .element("X")
                        .text(easyBody.body.getPosition().x)
//                        .text(0)
                        .pop()
                        .element("Y")
                        .text(easyBody.body.getPosition().y)
//                        .text(0)
                        .pop()
                        .element("Shape")
                        .text(easyBody.body.getFixtureList().get(0).getShape().getType())
                        .pop()
                        .element("Vertices");

                for (Vector2 vertex : easyBody.getVertices()) {
                    xmlVertices.element("Vertex").attribute("X", vertex.x).attribute("Y", vertex.y).pop();
                }

                xmlVertices.pop()
                        .element("BodyType")
                        .text(easyBody.body.getType())
                        .pop()
                        .element("Friction")
                        .text(easyBody.body.getFixtureList().get(0).getFriction())
                        .pop()
                        .element("Density")
                        .text(easyBody.body.getFixtureList().get(0).getDensity())
                        .pop()
                        .element("Restitution")
                        .text(easyBody.body.getFixtureList().get(0).getRestitution())
                        .pop()
                        .element("Awake")
                        .text(easyBody.body.isAwake())
                        .pop()
                        .element("Active")
                        .text(easyBody.body.isActive())
                        .pop()
                        .element("Angle")
                        .text(easyBody.body.getAngle())
                        .pop()
                        .pop();
                xmlWriter.flush();
            }
        }
        xmlWriter.pop();
        xmlWriter.flush();
        worldExportedSpriteAlpha = 1f;
        worldExportedPromptSprite.setAlpha(worldExportedSpriteAlpha);
    }
}
