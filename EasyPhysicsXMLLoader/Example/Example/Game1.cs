using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FarseerPhysics.Dynamics;
using EasyPhysicsXMLLoader;
using FarseerPhysics.DebugView;
using FarseerPhysics.Samples.ScreenSystem;
using FarseerPhysics;

namespace Example
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        DebugViewXNA view;
        World world;
        Camera2D camera;
        Body ball;
        SpriteFont font;
        bool isGrounded = false;
        List<Body> bodiesToDrop = new List<Body>();
        List<TimeSpan> elapsedTime = new List<TimeSpan>();
        List<TimeSpan> timeToFall = new List<TimeSpan>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;
            // TODO: Add your initialization logic here
            world = Loader.CreateWorld("Demo.xml");
            world.Gravity = new Vector2(0, 9.81f);
            view = new DebugViewXNA(world);
            view.LoadContent(GraphicsDevice, Content);
            camera = new Camera2D(GraphicsDevice);
            camera.Zoom = .5f;

            ball = Loader.GetBody(world, "ball");
            ball.BodyType = BodyType.Dynamic;

            ball.OnCollision += new OnCollisionEventHandler(ball_OnCollision);
            ball.OnSeparation += new OnSeparationEventHandler(ball_OnSeparation);
            base.Initialize();
        }

        void ball_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            isGrounded = false;
            if (fixtureB.Body.UserData.ToString().Contains("ramp"))
            {
                //bodiesToDrop.Add(fixtureB.Body);
                //elapsedTime.Add(TimeSpan.Zero);
                //timeToFall.Add(TimeSpan.FromMilliseconds(950/(Math.Abs(fixtureA.Body.LinearVelocity.X)+Math.Abs(fixtureA.Body.LinearVelocity.Y))));
            }
        }

        bool ball_OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            isGrounded = true;
            return true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("Font");

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        KeyboardState lastks;
        protected override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();

            if (ks.IsKeyUp(Keys.P))
            {
                world.Step(1 / 60f);
                camera.Position = ConvertUnits.ToDisplayUnits(ball.Position);
                camera.Jump2Target();
                if (ks.IsKeyDown(Keys.Space) && lastks.IsKeyUp(Keys.Space) && isGrounded)
                {
                    ball.ApplyLinearImpulse(new Vector2(0, -.15f));
                }
                
                for (int i = 0; i < bodiesToDrop.Count; i++)
                {
                    elapsedTime[i] += gameTime.ElapsedGameTime;
                    if(elapsedTime[i] >=  timeToFall[i])
                    {
                        elapsedTime.RemoveAt(i);
                        bodiesToDrop[i].BodyType = BodyType.Dynamic;
                        bodiesToDrop.RemoveAt(i);
                        i--;
                    }
                }
            }

            lastks = ks;

            // TODO: Add your update logic here
            camera.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            view.RenderDebugData(camera.SimProjection, camera.SimView);
            spriteBatch.Begin(SpriteSortMode.Texture, BlendState.AlphaBlend, null, null, null, null, camera.View);

            Loader.Draw(world, spriteBatch, font);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
