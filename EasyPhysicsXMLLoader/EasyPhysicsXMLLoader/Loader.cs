using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FarseerPhysics.Dynamics;
using System.Xml;
using FarseerPhysics.Factories;
using FarseerPhysics;

namespace EasyPhysicsXMLLoader
{
    public enum Shape
    {
        Polygon,
        Circle
    }

    public static class Loader
    {
        public static Dictionary<World, Dictionary<string, Body>> Bodies = new Dictionary<World, Dictionary<string, Body>>();

        public static World CreateWorld(string xml)
        {
            World world = new World(new Vector2(0, 9.81f));
            Bodies.Add(world, new Dictionary<string, Body>());
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(xml);
            }
            catch
            {
                throw new XmlException("Xml File not found: " + xml);
            }
            foreach (XmlElement bodyElement in document.GetElementsByTagName("Body"))
            {
                string name = bodyElement.GetElementsByTagName("Name")[0].InnerText;
                Vector2 position = new Vector2(float.Parse(bodyElement.GetElementsByTagName("X")[0].InnerText), -float.Parse(bodyElement.GetElementsByTagName("Y")[0].InnerText));
                List<Vector2> vertices = new List<Vector2>();
                Shape shape = (Shape)Enum.Parse(typeof(Shape), bodyElement.GetElementsByTagName("Shape")[0].InnerText);
                float radius = 0;
                float friction = 0;
                if (shape == Shape.Polygon)
                {
                    foreach (XmlElement vertexElement in bodyElement.GetElementsByTagName("Vertex"))
                    {
                        vertices.Add(new Vector2(float.Parse(vertexElement.GetAttribute("X").ToString()), -float.Parse(vertexElement.GetAttribute("Y").ToString())));
                    }
                }
                else if (shape == Shape.Circle)
                {
                    radius = float.Parse(bodyElement.GetElementsByTagName("Radius")[0].InnerText);
                }
                if (shape == Shape.Circle)
                {
                    friction = float.Parse(bodyElement.GetElementsByTagName("AngularDamping")[0].InnerText);
                }
                else
                {
                    friction = float.Parse(bodyElement.GetElementsByTagName("Friction")[0].InnerText);
                }


                BodyType bodyType = (BodyType)Enum.Parse(typeof(BodyType), bodyElement.GetElementsByTagName("BodyType")[0].InnerText.Replace("Body", ""));

                float density = float.Parse(bodyElement.GetElementsByTagName("Density")[0].InnerText);

                float restitution = float.Parse(bodyElement.GetElementsByTagName("Restitution")[0].InnerText);

                bool awake = bool.Parse(bodyElement.GetElementsByTagName("Awake")[0].InnerText);
                bool active = bool.Parse(bodyElement.GetElementsByTagName("Active")[0].InnerText);

                float rotation = float.Parse(bodyElement.GetElementsByTagName("Angle")[0].InnerText);

                Body body = BodyFactory.CreateBody(world, position, rotation, name);
                
                Fixture fixture;
                if (shape == Shape.Circle)
                {
                    fixture = FixtureFactory.AttachCircle(radius, density, body, new Vector2(0, 0), name + "Fixture");
                }
                else if (shape == Shape.Polygon)
                {
                    fixture = FixtureFactory.AttachPolygon(new FarseerPhysics.Common.Vertices(vertices), density, body, name + "Fixture");
                }
                body.Awake = awake;
                body.BodyType = bodyType;
                body.Restitution = restitution;
                body.Friction = friction;
                Bodies[world].Add(name, body);
            }
            world.Step(1 / 60f);
            return world;
        }

        public static Body GetBody(World world, string name)
        {
            if (Bodies.ContainsKey(world))
            {
                if (Bodies[world].ContainsKey(name))
                {
                    return Bodies[world][name];
                }
                else
                {
                    throw new KeyNotFoundException("Could not find Body ('" + name + "') in world");
                }
            }
            else
            {
                throw new KeyNotFoundException("Could not find world, must of not been loaded by static CreateWorld(string xml) method");
            }
        }

        public static void Draw(World world, SpriteBatch spriteBatch, SpriteFont font)
        {
            foreach (Body body in world.BodyList)
            {
                spriteBatch.DrawString(font, body.UserData.ToString(), ConvertUnits.ToDisplayUnits(body.Position), Color.White);
            }
        }
    }
}
